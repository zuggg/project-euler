# timer                          
from time import time

########################################


import numpy as np

# Pour tester :
# A = np.array([
#     [131, 673, 234, 103, 18 ],
#     [201, 96 , 342, 965, 150],
#     [630, 803, 746, 422, 111],
#     [537, 699, 497, 121, 956],
#     [805, 732, 524, 37 , 331] ])
# Le chemin minimal vaut 994

A = np.loadtxt('p081_matrix.txt',dtype=int,delimiter=',')


# Pour trouver le chemin minimal, on procède de colonne en colonne. En partant
# de la première on cherche le chemin minimal pour accéder aux cases de la
# seconde en deux temps: d'abord, en autorisant seulement les mouvements vers le
# haut, ensuite en autorisant seulement les mouvements vers le bas. Le chemin
# minimal est le minimum des deux.
# Il suffit ensuite d'itérer le processus sur le reste des colonnes, et donc la
# complexité est O(n*m), où (n,m) sont les dimensions de A.


def min_path(A):
    '''Returns the minimal path in the matrix from left to right, moving up, down,
    and right'''
    (n,m) = A.shape
    col = A[:,0]

    for j in range(1,m-1):
        col_down = [col[0]+A[0,j]] + [0] * (n-1)
        for i in range(1,n):
            col_down[i] = A[i,j] + min( col[i], col_down[i-1])

        col_up = [0]*(n-1) + [col[n-1]+A[n-1,j]]
        for i in range(n-2,-1,-1):
            col_up[i] = A[i,j] + min( col[i], col_up[i+1])

        col = [ min(a,b) for (a,b) in zip(col_up,col_down) ]

    return(min(col + A[:,m-1]))

########################################

t0 = time()
print(min_path(A))
print("-- Temps écoulé :", time() - t0)
