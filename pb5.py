
import numpy as np
import math

########################################

# timer                          
from datetime import datetime
startTime = datetime.now()

########################################


N=101

# crible d'Ératosthène
primes=np.ones(N, bool)
primes[0],primes[1]=False, False

for p in range(2,math.ceil(math.sqrt(N))):
    if primes[p]==True:
        i=p
        while i*p < N:
            primes[i*p]=False
            i+=1
    else:
        continue




S=1

for i,p in enumerate(primes):
    if p==True:
        mult=i
        while mult*i<N:
            mult*=i
        S*=mult

print(S)
    


########################################

print("-- Temps écoulé :", datetime.now() - startTime)
