from time import time

################################################################################
# temps d'exécution : 16.520854711532593 s 
################################################################################

def fact(n):
    if n <= 1:
        return(1)
    else:
        return( n*fact(n-1) )

def f(n,val):
    return( sum( fact(int(d)) for d in str(n)))

def L(N):
    val = [fact(d) for d in range(10)]
    chain = [0]*3628800         # valeur maximale de f(n,val) pour n < 10**10
    for n in range(1,N):
        if not chain[n]:
#            n=3
            stack = [n]
            chain[n] = -1
            while chain[f(n,val)]==0:
                n=f(n,val)
                stack.append(n)
                chain[n]=-1
            if chain[f(n,val)] > 0:     # on connaît la longueur de ce qui reste
                l = chain[f(n,val)]
            else:                   # on est tombé dans un cycle inconnu
                idx = stack.index(f(n,val))
                l = len(stack) - idx  # longueur du cycle
                for _ in range(l):
                    n = stack.pop()
                    chain[n] = l 
            while stack:
                n = stack.pop()
                l += 1
                chain[n] = l
    return(chain[:N])



t0=time()

v=L(10**6)
m = max(v)

count = 0
for (i,l) in enumerate(v):
    if l==m:
        count += 1
print(count)
print("--Temps écoulé :", time()-t0,"s")
