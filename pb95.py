
from math import sqrt
from time import time

def longest_cycle(N):
    '''Yields the smallest member of the longest cycle with members < N'''
    # Pour calculer la somme des diviseurs de n, n < N, on peut utiliser un
    # crible. De la même manière que pour celui d'Érathostène, pas besoin
    # d'aller plus loin que sqrt(N), car tous les diviseurs > sqrt(N) peuvent
    # être obtenus grâce à un diviseur < sqrt(N).
    s = [0,0] + [1]*(N-1)

    imax = int(sqrt(N))

    for i in range(2, imax+1):
        s[i*i] += i
        for j in range( i+1, N//i):
            s[i*j] += i + j
            
    # On recherche maintenant les cycles dont tous les éléments sont < N. Pour
    # cela, on met à jour un vecteur dont l'entrée n est -1 si n n'est pas dans
    # un cycle, et la longueur du cycle sinon.
    cycle = [0]*N
    for i in range(2,N):
        if cycle[i]==0:
            stack = []
            while i<N and cycle[i] == 0:
                cycle[i] = -1
                stack.append(i)
                i = s[i]
            if i < N and cycle[i] == -1 and i in stack: # on a visité un nouveau cycle
                k = stack.index(i)
                l = len(stack) - k
                for j in range(k,len(stack)):
                    cycle[stack[j]] = l

    return( cycle.index(max(cycle)) )

t0 = time()
print(longest_cycle(10**6), time()-t0)

