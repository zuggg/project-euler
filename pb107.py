
import numpy as np

# timer                          
from datetime import datetime
startTime = datetime.now()

########################################


A=[]

with open("p107_network.txt","r") as setabc:
    for line in setabc:
        line=line.strip("\n").replace("-","0")
        a=[int(s) for s in line.split(",")]
        A.append(a)
setabc.close()

s1 = sum( sum(line) for line in A)//2 

# Les 0 traduisent une absence d'arrête
# A=[[0,16,12,21,0,0,0],
#    [16,0,0,17,20,0,0],
#    [12,0,0,28,0,31,0],
#    [21,17,28,0,18,19,23],
#    [0,20,0,18,0,0,11],
#    [0,0,31,19,0,0,27],
#    [0,0,0,23,11,27,0]]

# est_connexe renvoit True si le graphe représenté par la matrice de covalence A est connexe
def est_connexe(A):
    n = len(A)
    marque = [True]+[False]*(n-1)
    pile = [0]
    while pile != []:
        s = pile.pop()
        for j in range(n):
            if A[s][j] and not marque[j]:
                pile.append(j)
                marque[j]=True
    return(marque == [1]*n)

# tri_arretes donne la liste des indices correspondant à un tri croissant de A
def tri_arretes(A):
    n = len(A)
    arretes = []
    idx_arretes = []
    for i in range(n):
        for j in range(i,n):
            if A[i][j]:
                arretes.append(A[i][j])
                idx_arretes.append([i,j])
    return( [idx_arretes[i] for i in np.argsort(arretes)] )


def graphe_minimal(A):
    idx_arretes = tri_arretes(A)
    while idx_arretes != []:
        [i,j] = idx_arretes.pop()
        poids = A[i][j]
        A[i][j], A[j][i] = 0, 0
        if not est_connexe(A):
            A[i][j], A[j][i] = poids, poids
    return

graphe_minimal(A)

s2 = sum( sum(line) for line in A)//2

print(s1-s2)



########################################

print("-- Temps écoulé :", datetime.now() - startTime)
