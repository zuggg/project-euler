
import numpy as np

# timer                          
from datetime import datetime
startTime = datetime.now()


########################################

# idée d'algorithme pour tester que les sommes des sous-ensembles de même cardinal sont différentes
#
#
# pour k entre 2 et n/2
#   pour 2k éléments parmi les n
#     pour les mauvais parenthèsages parmi 2k
#       si sum(A)=sum(B) alors break
#
#
# on peut établir la liste des mauvais parenthèsages, on peut faire
#
# pour k parmi 2k
#   si test
#     append

########################################

# parmi(n) renvoit un tableau de taille n*n tel que
# parmi(n)[i-1][j-1] contient les combinaisons de j éléments parmi range(i)

def parmi(n):
    combi=[]
    combi.append([[[0]]])
    for k in range(1,n):
        ancienne_ligne=combi[k-1]
        nouvelle_ligne=[]
        nouvelle_ligne.append(ancienne_ligne[0]+[[k]])
        for i in range(1,k):
            case = []
            for v in ancienne_ligne[i-1]:
                case.append(v+[k])
            case += ancienne_ligne[i]
            nouvelle_ligne.append(case)    
        nouvelle_ligne.append([[i for i in range(k+1)]])
        combi.append(nouvelle_ligne)
    return combi

# on établit une liste de mauvais parenthèsages
# mauvais_par[k-2] donne la liste des mauvais parenthèsages de k parmi 2k
def mauvais_pare(N):
    mauvais_par=[]

    for k in range(2,N//2+1):
        ligne=[]
        for c in combi[2*k-1][k-1]:
            if c[0]!=0:
                continue
            test=[-1]*2*k
            for i in range(k):
                test[c[i]]+=2
                b=False
                a=0
                for j in test:
                    a+=j
                    if a<0:
                        b=True
                        break
            if b:
                ligne.append(c)
        mauvais_par.append(ligne)
    return(mauvais_par)

# test_sssx teste une des conditions pour que le vecteur corresponde à un ensemble à somme spéciale
# toutes les fonctions prennent en argument un vecteur déjà trié
# test_sss1 renvoit true si la fonction sum est strictement croissante du cardinal 
# test_sss2 renvoit true si la fonction sum est injective sur les paires d'ensembles de même cardinal
# test_sss3 renvoit true si le vecteur ne comprend pas de doublon

def test_sss1(v):
    n=len(v)
    return( sum( v[:(n+1)//2 ] ) - sum( v[n//2+1:] ) > 0 )

def test_sss2(v):
    n=len(v)
    for k in range(2,n//2+1):
        for c in combi[n-1][2*k-1]:
            for p in mauvais_par[k-2]:
                s1=sum( v[c[j]] for j in p)
                s2=sum( v[i] for i in c )
                if 2*s1==s2:
                    # print([v[c[j]] for j in p])
                    # print([v[i] for i in c])
                    return(False)
    return(True)

def test_sss3(v):
    for i in range(len(v)-1):
        if v[i]==v[i+1]:
            return(False)
    return(True)

# A = np.loadtxt('p105_sets.txt',dtype=int,delimiter=',')
# np.loadtxt ne fonctionne pas car le nombre d'entrées par ligne est variable
A=[]
with open("p105_sets.txt","r") as setabc:
    for line in setabc:
        line=line.strip("\n")
        a=[int(s) for s in line.split(",")]
        A.append(a)
setabc.close()
        
N=max(len(v) for v in A) # taille maximale des vecteurs à tester

combi=parmi(N)
mauvais_par=mauvais_pare(N)

cnt=0
S=0
for v in A:
    v.sort()
    #print(v)
    if test_sss1(v) and test_sss3(v) and test_sss2(v):
        cnt+=1
        S+=sum(v)
    
print("Il y a",cnt,"ensembles à somme spéciale, pour une somme totale de",S)


########################################

print("-- Temps écoulé :", datetime.now()-startTime)
