
from primes import factor, gcd
from time import time

# Observations : pgcd(i,j) = 1 ssi pgcd(i,i-j) = 1 ssi pgcd(j,i-j) = 1, donc il
# suffit de tester un seul des pgcd. En outre, pgcd(i,j) = pgcd(rad[i],rad[j]).
# Finalement, si i et j sont premiers entre eux, alors rad[i*j]=rad[i]*rad[j],
# ce qui est important pour le dernier test (pas besoin de calculer rad jusqu'à
# 1/4*n^3).

def radicals(n):
    rad = [1]*(n+1)
    for i in range(1,n):
        if rad[i] == 1:    # c'est un nombre premier
            for j in range(i,n,i):
                rad[j] *= i
    return(rad)


t0 = time()
n = 1000
rad = radicals(n)

s = 0
for i in range(2,n):
    for j in range(1,(i-1)//2):
        if gcd(rad[i],rad[j]) == 1:
            if rad[i]*rad[i-j]*rad[j] < i:
                #print(i,i-j,j)
                s += i
                
print(s, time()-t0)

# L'algorithme semble être quadratique, donc on peut s'attendre un temps de
# calcul de plus d'une heure pour n = 120 000. Il faut trouver quelque chose de
# plus efficace.

################################################################################
# On teste d'abord la condition sur les radicaux :

t0 = time()
s = 0
for i in range(2,n):
    for j in range(1,min(i,n-i)):
        if rad[i]*rad[j]*rad[i+j] < i+j:
            if gcd(rad[i],rad[j])==1:
                s += i+j
print(s, time()-t0)

# C'est déjà beaucoup plus efficace, mais toujours quadratique et probablement
# ~30 minutes de calculs.

################################################################################
# On profite du fait que si i et j sont premiers entre eux et plus grands que 1,
# alors rad[i]*rad[j] ≥ 6. De plus, i et i-1 sont toujours premiers entre eux.

t0 = time()

n = 10000
rad = radicals(n)

s = 0
for i in range(3,n):
    if rad[i-1]*rad[i] < i:
        s += i
    if 6*rad[i] < i:
        for j in range(2,(i-1)//2):
            if rad[i]*rad[i-j]*rad[j] < i:
                if gcd(rad[i],rad[j]) == 1:
                #print(i,i-j,j)
                    s += i
                
print(s, time()-t0)

# Le temps de calcul reste grand : environ 200 s.

################################################################################
# Au lieu de parcourir tous les nombres, on parcourt la liste triées des
# radicaux. Temps d'exécution ~13 s.

from math import sqrt

    
def abchits(n):

    rad = radicals(n)

    # srad est la liste triée des radicaux
    srad = sorted( enumerate(rad), key=lambda x: x[1])[1:]
    
    # nextrad[r] contient le premier indice du radical suivant dans la liste triée
    # des radicaux
    nextrad = {}                   
    for i in range(len(srad)-1):
        if srad[i+1][1] > srad[i][1]:
            nextrad[srad[i][1]] = i+1

    s = 0

    lim = int(sqrt(n//2))
    i, r = 1, 0
    while r < lim :
        a, r = srad[i]
        j = nextrad[r]
        b, r2 = srad[j]
        lim2 = n//(2*r)
        while r2 < lim2:
            if  a+b < n:
                if r*r2*rad[a+b] < a+b and gcd(r,r2)==1:
                    s += a+b
                j += 1
            else:
                j = nextrad[r2]
            b, r2 = srad[j]
        i += 1

    for i in range(3,n):
        if rad[i-1]*rad[i] < i:
            s += i
            
    return(s)


t0 = time()
n = 10000
print(abchits(n), time()-t0)

