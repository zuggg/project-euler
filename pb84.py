# timer                          
from time import time

########################################

# L'idée est de simuler un jeu de monopoly, jusqu'à savoir quelles cases sont
# les plus visitées.
# On code les différentes cases par des entiers. GO désigne la case de départ,
# CC les cartes communauté, CH les cartes chance, R les gares, U les compagnies
# d'utilité, JAIL la prison, G2J "allez en prison", FP parking gratuit, et A-H
# les différentes rues.
# 
# 0 = GO        10 = JAIL       20 = FP        30 = G2J
# 1 = A1        11 = C1         21 = E1        31 = G1
# 2 = CC1       12 = U1         22 = CH2       32 = G2
# 3 = A2        13 = C2         23 = E2        33 = CC3
# 4 = T1        14 = C3         24 = E3        34 = G3
# 5 = R1        15 = R2         25 = R3        35 = R4
# 6 = B1        16 = D1         26 = F1        36 = CH3  
# 7 = CH1       17 = CC2        27 = F2        37 = H1  
# 8 = B2        18 = D2         28 = U2        38 = T2  
# 9 = B3        19 = D3         29 = F3        39 = H2  

from random import randint
from numpy.random import permutation
import matplotlib.pyplot as plt

def draw_chance(c,pos):
    if c==0:
        return(0)
    elif c==1:
        return(10)
    elif c==2:
        return(11)
    elif c==3:
        return(24)
    elif c==4:
        return(39)
    elif c==5:
        return(5)
    elif c==6 or c==7:          # gare suivante
        if pos==7:
            return(15)
        elif pos==22:
            return(25)
        else:
            return(5)
    elif c==8:
        if pos==22:
            return(28)
        else:
            return(12)
    elif c==9:
        return(pos-3)
    else:
        return(pos)
    
def draw_community(c,pos):
    if c==0:
        return(0)
    elif c==1:
        return(10)
    else:
        return(pos)

def partie_test(N):
    n=6                             # nombre de faces des dés

    ch_stack = permutation(16)      # on mélange les cartes
    ch_marker = 0                   # marque la carte actuelle
    cc_stack = permutation(16)
    cc_marker = 0

    position = 0                    # position actuelle
    double_counter=0                # nombre de doubles consécutifs

    count = [1] + [0]*39            # compteur des cases visitées

    i = 0                           # itérations
    while i<N:
        roll = (randint(1,n), randint(1,n))
        print(i,":",roll)
        if roll[0] == roll[1]:
            double_counter += 1
        else:
            double_counter = 0

        if double_counter == 3:
            print("Trois doubles consécutifs => prison")
            double_counter = 0
            position = 10
        else: 
            position = (position + sum(roll))%40

            if position == 30:
                print("Allez en prison")
                position = 10
            elif position == 7 or position == 22 or position == 36:
                print("Carte chance en position",position,":",ch_stack[ch_marker])
                position = draw_chance(ch_stack[ch_marker],position)
                ch_marker = (ch_marker + 1)%16
            if position == 2 or position == 17 or position == 33:
                print("Carte communauté en position",position,":",cc_stack[cc_marker])
                position = draw_community(cc_stack[cc_marker],position)
                cc_marker = (cc_marker + 1)%16

        count[position] += 1
        print("Position à la fin du tour :",position)
        i+=1
    return


def roll_N_times(N):
    global ch_stack, cc_stack, position, double_counter, ch_marker, cc_marker, count, n
    for _ in range(N):
        roll = (randint(1,n), randint(1,n))
        if roll[0] == roll[1]:
            double_counter += 1
        else:
            double_counter = 0

        if double_counter == 3:
            double_counter = 0
            position = 10
        else: 
            position = (position + sum(roll))%40

            if position == 30:
                position = 10
            elif position == 7 or position == 22 or position == 36:
                position = draw_chance(ch_stack[ch_marker],position)
                ch_marker = (ch_marker + 1)%16
            if position == 2 or position == 17 or position == 33:
                position = draw_community(cc_stack[cc_marker],position)
                cc_marker = (cc_marker + 1)%16

        count[position] += 1


    T = sum(count)
    freq = [p/T for p in count]
    
    return(freq)
#        return(count,ch_marker,cc_marker,position,double_counter)  


def plot_cont(N,Nmax):
    global ch_stack, cc_stack, position, double_counter, ch_marker, cc_marker, count, n
    ch_stack = permutation(16)      # on mélange les cartes
    cc_stack = permutation(16)
    position = 0                    # position actuelle
    double_counter = 0                # nombre de doubles consécutifs
    ch_marker = 0                   # marque la carte actuelle
    cc_marker = 0
    count = [1] + [0]*39            # compteur des cases visitées
    
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    x=[c for c in range(40)]

    xmax=Nmax//N
    
    def update(i):
        freq = roll_N_times(N)
        print("Cases fréquentes après",sum(count),"tours :",sorted(range(len(freq)), key=freq.__getitem__,reverse=True)[:5])

        ax.clear()
        ax.bar(x, freq)
        
    a = anim.FuncAnimation(fig, update, frames=xmax, repeat=False)
    plt.show()

def game_stats(N):
    global ch_stack, cc_stack, position, double_counter, ch_marker, cc_marker, count, n
    ch_stack = permutation(16)      # on mélange les cartes
    cc_stack = permutation(16)
    position = 0                    # position actuelle
    double_counter = 0              # nombre de doubles consécutifs
    ch_marker = 0                   # marque la carte actuelle
    cc_marker = 0
    count = [1] + [0]*39            # compteur des cases visitées
    freq = roll_N_times(N)
    print("Après",N,"tours, les cases les plus fréquentes sont :")
    top_freq = sorted(freq,reverse=True)[:4]
    top_idx  = sorted(range(len(freq)), key=freq.__getitem__,reverse=True)[:4]
    for i in range(4):
        print("Case",top_idx[i],":",100*top_freq[i],"%")


    
################################################################################

plot_cont(10**4,10**6)

n=6                             # nombre de faces des dés
game_stats(10**7)

