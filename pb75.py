from time import time

################################################################################
# On utilise le fait que le triplet (x,y,z), x impair, est un triplet
# pythagoricien primitif ssi il existe (p,q) qui vérifie:
# p>q, pgcd(p,q)=1, pq est pair, et (x,y,z)=(p^2-q^2,2pq,p^2+q^2)

def gcd(a,b):
    if a%b==0:
        return(b)
    else:
        return(gcd(b,a%b))

def triangles(L):
    '''Returns a list containing the number of right angle triangles for each length
    <= L'''
    count=[0]*((L+1)//2) 
    from math import sqrt
    for p in range(2, int(sqrt(L/2))+1 ):
        qmax = min(L//(2*p)-p+1, p)  # on veut que 2*p*(p+q) <= L
        for q in range(p%2+1, qmax, 2):
            if gcd(p,q)==1:
                l = p*(p+q)
                for i in range(l,(L+1)//2,l):
                    count[i] += 1
    return(count)


t0=time()
L=1500000
print( sum( p==1 for p in triangles(L)))
print("--Temps écoulé :", time()-t0,"s")
