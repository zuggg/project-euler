
from time import time
from primes import generate_primes
from math import sqrt

# Brute force, rien de particulier à expliquer. Je ne sais pas s'il y a des
# propriétés arithmétiques intéressantes, ce qui est sûr, c'est que la
# décomposition (si elle existe) n'est pas unique :
# 3^3 + 9^2 = 2^3 + 10^2
# Par ailleurs, calculer les puissances des nombres premiers et les stocker est
# économique en temps, et peu coûteux en mémoire dans ce problème.

t0=time()
N = 50*10**6

N2, N3, N4 = N**(1/2), N**(1/3), N**(1/4)
plist=generate_primes(int(N2))

plist2 = [p**2 for p in plist]
plist3, plist4 = [], []

i=0
while plist[i] < N4:
    plist3.append(plist[i]**3)
    plist4.append(plist[i]**4)
    i+=1
while plist[i] < N3:
    plist3.append(plist[i]**3)
    i+=1

    
issum=[]
for p in plist2:
    for q in plist3:
        if p + q < N:
            for r in plist4:
                n = p + q + r
                if n < N:
                    issum.append(n)
                else:
                    break
        else:
            break

print(len(set(issum)))
print(time()-t0)


