
from time import time

# Soit p(n,k) le nombre de partitions de n en k parties (non nulles).
# Soit (x1, ... ,xk), une partition de n, c'est-à-dire
#    x1 ≥ x2 ≥ ... ≥ xk ≥ 1, n = sum xj.
# Alors, soit xk = 1, auquel cas (x1, ... ,x(k-1)) est une partition de n-1,
# soit xk > 1, auquel cas (x1-1, ... , xk-1) est une partition de n-k.
# On en déduit la formule de récurrence
#    p(n,k) = p(n-1,k-1) + p(n-k,k).

def partitions(N):
    '''Returns the list of the number of partitions of n in exactly k parts.'''    
    p=[[1],[1,1]]
    for n in range(2,N):
        new_line=[1]
        for k in range(1,(n+1)//2):
            new_line.append( p[n-1][k-1] + p[n-k-1][k] )
        for k in range((n+1)//2,n):
            new_line.append( p[n-1][k-1] )
        p.append(new_line+[1])
    return(p[N-1])

t0=time()
N=100
print("Nombre de partitions de",N,"en somme d'au moins deux entiers :",sum(partitions(N))-1)
print("Temps écoulé :",time()-t0,"s")
