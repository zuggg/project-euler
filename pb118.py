
from math import sqrt, ceil
from itertools import permutations, combinations
from time import time

################################################################################
# Prime stuff

def generate_primes(N, pmax):
    '''Returns the list of prime numbers < N using Erathostenes' sieve.'''
    primes=[True]*(N//2)

    primes[0] = False

    k = 1
    while (k < ceil(sqrt(N//2))):
        if primes[k]:
            for i in range(2*k*(k+1), N//2, 2*k + 1):
                primes[i]=False
        k += 1

    primes_list=[2]
    for k,b in enumerate(primes):
        if 2*k+1>pmax:
            break
        if b:
            primes_list.append(2*k+1)

    return(primes_list, primes)

def isprime(n,primes_list,primes):
    if n == 2:
        return(True)
    
    if n<2 or n%2==0:
        return(False)

    if n < 2*len(primes):
        return(primes[(n-1)//2])
    
    lim = int(sqrt(n))
    for p in primes_list:
        if n==p or p>lim:
            return(True)
        if n%p == 0:
            return(False)
    else:
        return("Could not determine primality")

################################################################################
    
def partitions(n):
    '''Yields the partitions of n'''
    a = [n]
    yield(a)
    while a != [1]*n:
        k = 0
        while a[k] == 1:
            k += 1
        q, r = (k+1)//(a[k]-1), (k+1)%(a[k]-1)
        if r:
            a = [r] + [a[k]-1]*(q+1) + a[(k+1):]
        else:
            a = [a[k]-1]*(q+1) + a[(k+1):]
        yield(a)

def list2digit(n):
    return( int( "".join([ str(d) for d in n]) ) )

def pandigital_nums( dlist ):
    '''Yields all the numbers made from digits in dlist.'''
    for n in permutations(dlist):
        yield( list2digit(n) )
                
################################################################################
# Méthode 1 : temps d'exécution 100 s
################################################################################
# Algorithme naïf. On génère les partitions de 9, qui correspondent à des
# concaténation de l'ensemble {1,2,3,4,5,6,7,8,9}. Par exemple, [1,1,2,2,3]
# donne {1,2,34,56,789}. On parcourt ensuite toutes les permutations de ces
# chiffres, en ne gardant que celles qui correspondent à une suite
# croissante. Finalement, on teste la primalité des membres de l'ensemble.

# Si n a 9 chiffres et est pandigital, alors n est divisible par 3, donc on peut
# déjà écarter ces nombres.

t0 = time()
# Test pour les ensembles de la forme {d,n}, avec 10^7 < n < 10^8
plist, primes = generate_primes(10**7, 10**4)

count = 0

# Manière pas très efficace de lister tous les ensembles possibles, étant donnée
# une répartition des chiffres

def pandigital_sets( dlist, part ):
    cut = [0] + [ sum( part[:k+1]) for k in range(len(part)) ]
    for n in permutations(dlist):
        L = [ list2digit( n[cut[k]:cut[k+1]] ) for k in range(len(cut)-1) ]
        if sorted(L) == L:
            yield(L)

dmax = 9
for part in partitions(dmax):
    if part != [9]:
        for L in pandigital_sets( range(1,dmax+1), part ):
            for n in L:
                if not isprime(n,plist,primes):
                    break
            else:
                count += 1

print(count,"\n", time() - t0 )
 
################################################################################
# Méthode 2 : temps d'exécution 3.9 s
################################################################################
# La méthode précédente est très coûteuse car on teste la primalité d'un même
# nombre beaucoup de fois. Il est plus intéressant de développer un algorithme
# récurrent, par exemple avec du backtracking.
# Pour palier ce problème, on choisit ici de commencer par énumérer les
# partitions de {1,2,...,9}, puis de compter le nombre de premiers s'écrivant
# comme une permutation des chiffres d'un sous-ensemble.

def list_partitions(L):
    if len(L) == 1:
        yield [L]
    else:
        a = [L[0]]
        for p in list_partitions(L[1:]):
            yield [a] + p
            for i in range(len(p)):
                yield sorted(p[0:i] + [ a + p[i] ] + p[i+1:], key=len)
                
def count_primes(L):
    if sum(L)%3 == 0: # les nombres sont tous divisibles par 3
        return(0)
    count = 0
    for n in permutations(L):
        if isprime(list2digit(n), plist, primes):
            count += 1
    return(count)

####################

t0 = time()

count = 0
for p in list_partitions([d for d in range(1,10)]):
    tempcount = 1
    for L in p:
        tempcount *= count_primes(L)
        if tempcount == 0:
            break
    count += tempcount

print(count, time()-t0)
