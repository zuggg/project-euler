# timer                          
from datetime import datetime
startTime = datetime.now()

########################################


import numpy as np

# A = np.array([
#     [131, 673, 234, 103, 18 ],
#     [201, 96 , 342, 965, 150],
#     [630, 803, 746, 422, 111],
#     [537, 699, 497, 121, 956],
#     [805, 732, 524, 37 , 331] ])

A = np.loadtxt('p081_matrix.txt',dtype=int,delimiter=',')

# print(A[0][0])
# print(A[0][10])


for i in range(1,len(A)):
    A[0][i]+=A[0][i-1]
    A[i][0]+=A[i-1][0]

for i in range(1,len(A)):
    A[i][i] += min(A[i-1][i],A[i][i-1])
    for j in range(i+1,len(A)):
        A[i][j] += min(A[i-1][j],A[i][j-1])
        A[j][i] += min(A[j-1][i],A[j][i-1])
        
print(A[-1][-1])


########################################

print("-- Temps écoulé :", datetime.now() - startTime)
