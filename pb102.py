import numpy as np



# timer                          
from datetime import datetime
startTime = datetime.now()


########################################

triangles = np.loadtxt('p102_triangles.txt',dtype=int,delimiter=',')

# triangles=np.array([
#     [-340, 495, -153, -910, 835, -947],
#     [-175, 41 , -421, -714, 574, -645] ])


def contains_zero(T):
    nAB=[ T[3]-T[1], T[0]-T[2] ] # normale au côté AB
    nBC=[ T[5]-T[3], T[2]-T[4] ] # normale au côté BC
    nCA=[ T[1]-T[5], T[4]-T[0] ]

    # on vérifie que les produits scalaires on même signe
    qA=np.dot( nAB, [T[0],T[1]] )*np.dot( nCA, [T[0],T[1]] )
    qB=np.dot( nAB, [T[2],T[3]] )*np.dot( nBC, [T[2],T[3]] )
    qC=np.dot( nCA, [T[4],T[5]] )*np.dot( nBC, [T[4],T[5]] )
    
    if(qA>=0 and qB>=0 and qC>=0):
        return True
    else:
        return False

print(sum(contains_zero(T) for T in triangles))

########################################

print("-- Temps écoulé :", datetime.now()-startTime)
