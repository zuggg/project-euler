
def generate_primes(N):
    '''Returns the list of prime numbers < N using Erathostenes' sieve.'''
    from math import ceil, sqrt
    primes=[True]*(N//2)

    primes[0] = False

    k = 1
    while (k < ceil(sqrt(N//2))):
        if primes[k]:
            for i in range(2*k*(k+1), N//2, 2*k + 1):
                primes[i]=False
        k += 1

    primes_list=[2]
    for k,b in enumerate(primes):
        if b:
            primes_list.append(2*k+1)

    return(primes_list)

def nth_prime(n):
    '''Returns the n-th prime number.'''
    # Pour cela, on utilise un crible d'Ératosthène, le problème étant alors de
    # déterminer la taille du crible, on utilise des estimations quant à la
    # taille du N-ième premier, cf. https://primes.utm.edu/howmany.html#2
    from primes import generate_primes
    from math import floor, log
    
    if 0<n and n<13: # borne sup
        N =38
    elif n<8062:
        N = floor(n*(log(n)+log(log(n)) -1 + 1.8*log(log(n))/log(n)))
    elif n<15985:
        N = floor( n*(log(n)+log(log(n)) - 0.9385 ))
    else:
        N = floor( n*(log(n)+log(log(n)) - 0.9427 ))

    return(generate_primes(N)[n-1])
        
def factor(n):
    '''Returns the prime factorization of n = p1**a1 * p2**a2 *... in the form
    [[p1,a1],[p2,a2],...]'''
    from primes import generate_primes
    from math import sqrt
    
    lim = int(sqrt(n))
    plist = generate_primes(lim+1)
    factors = []
    
    for p in [p for p in plist if p <= lim]:
        if n%p == 0:
            n = n//p
            power = 1
            while n%p == 0:
                n = n//p
                power +=1
            factors.append([p,power])
            lim = int(sqrt(n))
    if n > 1:
        factors.append([n,1])
    return(factors)
    
def gcd(a,b):
    if a == 0: return(b)
    if b == 0: return(a)
    return( gcd(b,a%b) )
