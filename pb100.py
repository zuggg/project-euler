
# On cherche à résoudre
#    a/b * (a-1)/(b-1) = 1/2,
# autrement dit, en posant y = 2*a - 1, x = 2*b - 1,
#    x^2 - 2*y^2 = -1.
# Cette équation est une équation de Pell généralisée, et on sait que toutes les
# solutions peuvent être retrouvées à partir de la solution fondamentale (sous
# réserve d'existence de solution) c'est-à-dire celle qui, parmi les solutions
# { x,y > 0 }, minimise x.
# En particulier, si (x0, y0) est la solution minimale, alors pour toute autre
# solution (x,y), il existe n dans Z tel que
#    x + sqrt(2)*y = (x0 + sqrt(2)*y0)^n.
# D'autre part, toute solution positive (p,q) correspond à un convergent p/q du
# développement en fraction continue de sqrt(2) ; il suffit donc de tester les
# convergents successifs de sqrt(2) pour trouver la solution fondamentale. En
# l'occurence, on trouve (x0,y0) = (1,1), et les autres solutions sont alors
#   | x_n | =  | 3  2 |^2 | 1 |
#   | y_n |    | 4  3 |   | 1 |.
# On montre facilement que x_n et y_n sont impairs pour tout n, et donc
# correspondent aux solutions du problème initial.

# On cherche alors le plus petit n tq y_n > 2*10**12 - 1.
# Plutôt qu'une exponentiation naïve, on pourrait estimer l'exposant recherché
# grâce aux valeurs propres et utiliser une exponentiation rapide, mais ce n'est
# pas nécessaire ici.

from time import time


t0 = time()

N = 10**12
ymax = 2*N - 1
x, y = 1, 1

while y <= ymax:
    x, y = 3*x + 2*y, 4*x + 3*y

print((x+1)//2,time() - t0)
