import numpy as np
import math

########################################


# timer                          
from datetime import datetime

########################################

print("n=")
n=int(input())

t0 = datetime.now()

########################################
# On cherche le n-ième nombre premier. 
# Pour cela, on utilise un crible d'Ératosthène, le problème étant alors de déterminer la taille du crible,
# on utilise des estimations quant à la taille du N-ième premier, cf. https://primes.utm.edu/howmany.html#2

if 0<n and n<13: # borne sup
    N =38
elif n<8062:
    N = math.floor(n*(math.log(n)+math.log(math.log(n)) -1 + 1.8*math.log(math.log(n))/math.log(n)))
elif n<15985:
    N = math.floor( n*(math.log(n)+math.log(math.log(n)) - 0.9385 ))
else:
    N = math.floor( n*(math.log(n)+math.log(math.log(n)) - 0.9427 ))

# Nm=math.floor( n*(math.log(n)+math.log(math.log(n)) - 1)) # borne inf
    
# crible d'Ératosthène
primes=np.ones(N, bool)
primes[0],primes[1]=False, False

for p in range(2,math.ceil(math.sqrt(N))):
    if primes[p]==True:
        i=p
        while i*p < N:
            primes[i*p]=False
            i+=1
    else:
        continue

# vecteur de nombres premiers
primes_list=[]
for p,b in enumerate(primes):
    if b==True:
        primes_list.append(p)

t1 = datetime.now()

print("Le", str(n)+"ième nombre premier est",primes_list[n-1])

########################################

# Autre méthode, plus directe :

primes2=[2,3]
i=2

while i<n:
    is_prime=False
    cand=primes2[i-1]
    while is_prime==False:
        cand+=2
        is_prime=True
        for p in primes2:
            if cand%p==0:
                is_prime=False
                break
        if is_prime==True:
            primes2.append(cand)
            i+=1

t2 = datetime.now()


########################################

print("-- Temps écoulé : avec le crible", t1-t0)
print("         avec la méthode directe", t2-t1)

