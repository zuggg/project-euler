
from time import time

# On peut aborder le problème à l'aide d'un crible, puis utiliser les fonctions
# de tri de Python...

t0 = time()

n = 100000
k = 10000
crible = [1]*(n+1)

for i in range(1,n+1):
    if crible[i] == 1:    # c'est un nombre premier
        for j in range(i,n+1,i):
            crible[j] *= i

print( sorted(range(n+1), key=lambda i: crible[i] )[k] , "\n", time() - t0)
