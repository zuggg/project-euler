
########################################

from time import time
import numpy as np

########################################

# Le nombre de fractions réduites ayant pour dénominateur d est exactement le
# nombre d'entiers < d premiers avec d, càd phi(d). On utilise alors la formule
# phi(n)=n*prod_p (1-1/p), où p sont les nombres premiers qui apparaissent dans
# la décomposition de n pour remplir un crible avec les valeurs de phi.


def generate_phi_np(N):
    "Renvoit un array de taille N contenant les valeurs de la fonction d'Euler"
    phi = np.arange(N+1,dtype=np.int32)
    for i in range(2,N+1):
        if phi[i] == i:
            for j in range(i,N+1,i):
                phi[j] -= phi[j]//i
    return(phi)

def generate_phi(N):
    phi = [i for i in range(N+1)]
    for i in range(2,N+1):
        if phi[i] == i:
            for j in range(i,N+1,i):
                phi[j] -= phi[j]//i
    return(phi)

########################################

# liste des nombres premiers < N
def generate_primes(N):
    # crible d'Ératosthène
    primes=np.ones(N, bool)
    primes[0],primes[1]=False, False
    
    for i in range(2,N//2):
        primes[i*2]=False

    for p in range(3,ceil(sqrt(N))):
        if primes[p]:
            i=p
            while i*p < N:
                primes[i*p]=False
                i+=2
        else:
            continue
            
    primes_list=[]
    for p,b in enumerate(primes):
        if b:
            primes_list.append(p)
    return(primes_list)


def smallest_prime_factor(n):
    lim = int(sqrt(n))
    for p in plist:
        if p > lim:
            return(n)
        if n%p == 0:
            return(p)        

def phi(n):
    val = n
    for [p,m] in prime_decomp(n):
        val = val//p * (p-1)
    return val



########################################


t0 = time()


N = 10**6
phi_array = generate_phi(N)
print(sum( phi_array[2:] ))

    
print("-- Temps écoulé : ", time()-t0,"s")


# Pour une raison que je ne comprends pas, numpy est beaucoup plus lent que
# python de base (21s contre 1s)


nmb = 100
t0 = time()
for i in range(nmb):
    test = np.arange(N+1,dtype=np.int32)
t1 = time()
for i in range(nmb):
    test = [i for i in range(N+1)]
print(time()-t1, t1-t0)

# Pour 100 créations de liste,
# np arange   : 5.911692142486572 s
# python list : 0.08336377143859863 s

