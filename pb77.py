from time import time
from primes import generate_primes

# Brute force, pour tester sur des petits entiers

N=30
plist = generate_primes(N)

count=[0]*(N+1)
for i in range(1,N//2+1):
    for p in it.combinations_with_replacement(plist,i):
        if sum(p)<=N : count[sum(p)] += 1


# Formule de récurrence : soit n(k,pmax) le nombre de partitions de k en nombres
# premiers ≤ pmax. Alors on a :
#    n(k,pmax) = sum n(k-p,p) pour p ≤ pmax
# On peut grandement optimiser le programme ci-dessous en stockant les valeurs
# dans un tableau, mais c'est suffisament rapide pour les valeurs de ce
# problème.

def iterate_until(plist,pmax):
    i=0
    while plist[i] <= pmax:
        yield plist[i]
        i+=1
    return
    

def n(k,pmax=0):
    if pmax==0: pmax=k
    if (k==0):
        return(1)
    if (k==1 or k<0):
        return(0)
    elif (pmax==2):
        return(1-k%2)    
    else:
        return(sum( n(k-p,p) for p in iterate_until(plist,pmax) ))
    

t0=time()
N=100
plist = generate_primes(N)      # on génère suffisament de nombres premiers

i=1
while(n(i) < 5000):
    i+=1
print(i)
print("Temps écoulé :",time()-t0,"s")
