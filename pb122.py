
from time import time

# On construit un arbre de la manière suivante : la racine est 1. Pour chaque
# feuille a, on rajoute un enfant a + b pour chaque ancètre b de a (a y
# compris). On obtient par exemple :
#
#                                1
#                                2
#                   3                            4
#       4        5          6           5         6          8
#    5 6 7 8  6 7 8 10  7 8 9 12    6 7 9 10  7 8 10 12  9 10 12 16
#
# Le problème est que l'arbre croît extrèmement vite : le niveau k contient k!
# éléments, c'est donc très coûteux dès que k > 10. 

# On peut améliorer l'algorithme (en perdant cependant l'optimalité) en enlevant
# les nombres déjà obtenus à une profondeur strictement inférieure. Cependant,
# cette méthode ne permet pas de trouver les chemins optimaux pour tout n, le
# premier contre exemple étant 12509 (et donc l'algorithme suffirait amplement
# pour ce problème, mais j'ai choisi d'implémenter l'algorithme exact).

t0 = time()

nmax = 200

best_exp = {}
best_exp[1] = 0
best_exp[2] = 1

exponents = [[(1,0)] , [(2,0)]]
for k in range(1,10):
    newline = []
    for i in range(len(exponents[k])):
        (a, idx) = exponents[k][i]
        b = a
        if a + b <= nmax:
            newline.append( ( a + b, i) )
            if a+b not in best_exp:
                best_exp[a+b] = k+1
        for j in reversed(range(k)):
            (b, idx) = exponents[j][idx]
            if a + b <= nmax:
                newline.append( ( a + b, i) )
                if a+b not in best_exp:
                    best_exp[a+b] = k+1
    exponents.append(newline)

# Pas assez de mémoire pour obtenir le 11e niveau...
    
for i in range(1,201):
    if i not in best_exp:
        print(i)

# Seul 191 n'a pas été atteint : on peut donc remplir à la main.
        
best_exp[191] = 11

print(sum( best_exp[i] for i in range(1,201) ))
print(time() -t0)


################################################################################
# Méthode 2
################################################################################
# Exactement la même chose que la méthode 1, mais avec un parcours en profondeur
# plutôt qu'en largeur pour économiser la mémoire.

def DFS_exp(nmax, dmax, chain=[1,2]):
    global best_exp             # index des meilleurs exposants
    k = len(chain)
    if k > dmax:                # profondeur maximale
        return
    for d in chain:
        n = d + chain[-1]
        if n > nmax:            # tous les autres entiers seront > nmax
            return              # car chain est croissante, donc return
        if best_exp[n-1] > k:
            best_exp[n-1] = k
        DFS_exp(nmax, dmax, chain+[n])


t0 = time()

nmax = 200
dmax = 10
best_exp = [n for n in range(1,nmax+1)]
best_exp[0], best_exp[1] = 0, 1 
DFS_exp(nmax,dmax)

print(time()-t0)

# Un peu plus rapide, et surtout termine lorsqu'on choisit 11 comme profondeur
# maximale. Par contre, il n'y a pas moyen de prévoir le dmax approprié, ce qui
# constitue le désavantage par rapport à l'algorithme de parcours en largeur
# précédent.

################################################################################
# Méthode 3
################################################################################
# À nouveau un algorithme de parcours en largeur, mais cette fois-ci on coupe
# les branches avec des étapes non optimales. Cette méthode ne donne _pas_
# les résultats optimaux.

def BFS_star_exp(nmax,dmax):
    best_exp = [n for n in range(nmax+1)]
    best_exp[1] = 0
    best_exp[2] = 1

    exponents = [[(1,0)] , [(2,0)]]
    for k in range(1,dmax):
        newline = []
        for i in range(len(exponents[k])):
            (a, idx) = exponents[k][i]
            b = a
            if a + b <= nmax and best_exp[a + b] > k:
                newline.append( ( a + b, i) )
                best_exp[a+b] = k + 1
            for j in reversed(range(k)):
                (b, idx) = exponents[j][idx]
                if a + b <= nmax and best_exp[a + b] > k:
                    newline.append( ( a + b, i) )
                    best_exp[a+b] = k+1
        exponents.append(newline)
    return(best_exp)

t0 = time()
print(sum( BFS_star_exp(1000,20)),"\n", time() - t0)

# L'avantage de cette méthode est de trouver des chemins presque optimaux avec
# une complexité raisonnable.

################################################################################

# Estimation de la perte d'efficacité par rapport à une exponentiation rapide
# classique

def binary_exp(n):
    b = bin(n)[2:]
    return( len(b) + sum( int(d) for d in b ) - 2 )

print(sum( binary_exp(n) for n in range(2, 1001)))

# Pour nmax = 1000, le nombre total d'opérations pour la méthode binaire moins
# le nombre d'opération minimal est égal à
#    11925 - 10808 = 1117,
# ... donc la méthode binaire est quand même plutôt efficace !
