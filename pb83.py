# timer                          
from time import time

################################################################################

import numpy as np

# Pour tester :
# A = np.array([
#     [131, 673, 234, 103, 18 ],
#     [201, 96 , 342, 965, 150],
#     [630, 803, 746, 422, 111],
#     [537, 699, 497, 121, 956],
#     [805, 732, 524, 37 , 331] ], dtype=np.int32)
# Le chemin minimal vaut 2297

A = np.loadtxt('p081_matrix.txt',dtype=np.int32,delimiter=',')

################################################################################
# Méthode 1 : algorithme d'exploration du graphe. On commence par créer un
# vecteur distance, qui donne la distance minimale entre un sommet v et un
# sommet de référence v0 (ici la première case de la matrice). Ce vecteur est
# initialisé à 0 en v0 et +inf partout ailleurs. On crée aussi une pile, dans
# laquelle il y a v0.
# Pour chaque sommet v de la pile :
#    pour chaque voisin w de v :
#       si distance(w) > distance(v) + d(v,w) (où d(v,w) est le poids),
#           alors distance(w) = distance(v) + d(v,w) et pile = pile U {w}

def neighbors(i,j,n,m):
    if i==0:                    # top border or corner
        if j==0:
            return([0,1],[1,0])
        elif j==m-1:
            return([0,m-2],[1,m-1])
        else:
            return([0,j-1],[0,j+1],[1,j])
    elif i==n-1:                # bottom border or corner
        if j==0:
            return([n-1,1],[n-2,0])
        elif j==m-1:
            return([n-2,m-1],[n-1,m-2])
        else:
            return([n-1,j-1],[n-1,j+1],[n-2,j])
    elif j==0:                  # left border
        return([i-1,0],[i+1,0],[i,1])
    elif j==m-1:                # right border
        return([i-1,m-1],[i+1,m-1],[i,m-2])
    else:
        return([i-1,j],[i+1,j],[i,j-1],[i,j+1])
    

def min_path(A):
    '''Returns the minimal path from top left to bottom right, moving up, down,
    right, and left.'''
    (n,m) = A.shape
    B = 2147483647* np.ones([n,m], dtype=np.int32)  # on initialise B avec +inf
    B[0,0] = A[0,0]
    
    stack=[[0,0]]
    
    while stack:
        [a,b]=stack.pop()
        for [x,y] in neighbors(a,b,n,m):
            if B[x,y] > A[x,y] + min( B[v,w] for [v,w] in neighbors(x,y,n,m)):
                B[x,y] = A[x,y] + min( B[v,w] for [v,w] in neighbors(x,y,n,m))
                if [x,y] not in stack:
                    stack.append([x,y])

    return(B)
            
########################################

t0 = time()
print(min_path(A))
print("-- Temps écoulé :", time() - t0,"s")


################################################################################
# Méthode 2
################################################################################
# Implémentation de l'algorithme de Dijkstra
# (https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm)
# L'algorithme est assez proche de l'algorithme précédent, mais on ne visite
# chaque case qu'une seule fois, et on peut s'arrêter dès que la case cible est
# atteinte.

def djikstra(A):
    (n,m) = A.shape
    dist = 2147483647*np.ones([n,m], dtype=np.int32)  
    dist[0,0] = A[0,0]

    # Initialisation
    to_visit = {}
    to_visit[0,0] = dist[0,0]
    i,j=0,0
    
    while (i,j)!=(n-1,m-1):
        i,j = min(to_visit, key=to_visit.get)
        d = to_visit.pop((i,j))
        dist[i,j] = d
        for [x,y] in neighbors(i,j,n,m):
            new_val = d + A[x,y]
            if new_val < dist[x,y]:
                dist[x,y] = new_val
                to_visit[x,y] = new_val

    return(dist[-1,-1])

t0 = time()
print(djikstra(A))
print("-- Temps écoulé :", time() - t0,"s")
