

from time import time

t0=time()

f = [1,1,1,1]
for n in range(3,51):
    f.append( 1 + sum( f[n-k]*(k-2) for k in range(3,n+1) ) ) 

print(f[51], time()-t0)

