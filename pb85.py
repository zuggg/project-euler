
# Dans le rectangle de taille n×m, le nombre de rectangles de taille i×j est
# égal à (n+1-i)*(m+1-j). Par conséquent, le nombre total de rectangles dans le
# rectangle de taile n×m est
#    N(n,m) = sum (n+1-i)*(m+1-j)
#           = (sum i)*(sum j)
#           = n*(n+1)*m*(m+1)/4
# On parcourt la liste des candidats pour minimiser |2000000-N(n,m)|

from math import sqrt

N=2000000
mmax = int(sqrt(2*N))

triangle = [ (n*(n+1))//2 for n in range(1,mmax+1)]

best = N
for m in range(len(triangle)):
    for n in range(m+1):
        rec_num = triangle[n]*triangle[m]
        if abs(N-rec_num) < 50:
            print(n+1,m+1,rec_num)
            #best = abs(N-rec_num)
            #(a,b)=(n+1,m+1)
        if (rec_num > N):
            break

print(a*b)
