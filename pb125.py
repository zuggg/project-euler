
# Une approche brute force, en pré-calculant les carrés.

def is_palindrome(n):
    return( str(n) == str(n)[::-1] ) 

def find_palindromes(N):
    from math import sqrt
    pal = []
    lim = int(sqrt(N//2))
    squares = [ i**2 for i in range(1,lim+1) ]
    for i in range(lim-2):
        s = squares[i]
        for j in range(i+1,lim-1):
            s += squares[j]
            if s > N: break
            if is_palindrome(s):
                pal.append(s)
    return(pal)

from time import time

t0 = time()
N = 10**8
print( sum(set(find_palindromes(N))), "\n--", time() - t0,"s")
