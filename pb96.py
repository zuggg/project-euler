
from itertools import product
from time import time
from copy import deepcopy

################################################################################
# Itérateurs utiles
################################################################################

def region(i,j):
    '''Yields all other cells in the region of cell (row + column + box)'''
    a, b = 3*(i//3), 3*(j//3)
    for k in range(9):
        if k!=i:
            yield (k,j)
        if k!=j:
            yield (i,k)
    for (k,l) in product(range(a,a+3), range(b,b+3)):
        if k!=i and l!=j:
            yield(k,l)

def row(i,j):
    '''Yields the coordinates of other cells in the same row'''
    for k in range(9):
        if k != j:
            yield((i,k))

def column(i,j):
    '''Yields the coordinates of other cells in the same column'''
    for k in range(9):
        if k != i:
            yield((k,j))

def box(i,j):
    '''Yields the coordinates of other cells in the 3x3 box'''
    a, b = 3*(i//3), 3*(j//3)
    for k in range(3):
        for l in range(3):
            if (k,l) != (i,j):
                yield((a+k,b+l))

################################################################################
# Sudoku class
################################################################################

class sudoku:
    def __init__(self, data):
        self.grid = deepcopy(data)
        self.stack = []         # stack contains cells with only one candidate
        
    def show(self):
        '''Prints the formatted grid'''
        def lineprint(line):
            formatted_line=""
            i = 0
            for d in line:
                if i%3 == 0: formatted_line += "│ "
                if d==0: formatted_line += "⋅ "
                else: formatted_line += str(d)+" "
                i +=1
            print(formatted_line+"│")
        i = 0
        print("┌───────┬───────┬───────┐")
        for l in self.grid:
            if i == 3 or i ==6 : print("├───────┼───────┼───────┤")
            lineprint(l)
            i+=1
        print("└───────┴───────┴───────┘")

    def show_candidates(self):
        '''Prints a formatted grid of candidates'''
        def lineprint(line):
            formatted_line=""
            i = 0
            for s in line:
                if i%3 == 0: formatted_line += "│ "
                cell = "{:^9}".format("".join( str(d) for d in s))
                formatted_line += cell+" "
                i +=1
            print(formatted_line+"│")
        i = 0
        sep = "─"*31
        print("┌"+sep+"┬"+sep+"┬"+sep+"┐")
        for l in self.candidates:
            if i == 3 or i ==6 : print("├"+sep+"┼"+sep+"┼"+sep+"┤")
            lineprint(l)
            i+=1
        print("└"+sep+"┴"+sep+"┴"+sep+"┘")
    
        
    def update_cell(self,i,j,d):
        '''Updates grid cell (i,j) with value d and candidates accordingly'''
        self.grid[i][j] = d
        self.candidates[i][j] = set([d])
        self.stack.append((i,j))
        self.count -= 1
                    
    def init_candidates(self):
        '''Initializes the candidates matrix.'''
        self.count = 81
        self.candidates = [[set(range(1,10)) for _ in range(9)] for _ in range(9)]
        for (i,j) in product(range(9),repeat=2):
            if self.grid[i][j] != 0:
                self.candidates[i][j] = set([self.grid[i][j]])
                self.stack.append((i,j))  
                self.count -= 1

    def only_in_region(self,i,j):
        '''Returns True and d if cell (i,j) is the only cell in row, column or box to contain candidate d.'''
        if self.grid[i][j] == 0:
            s = self.candidates[i][j].copy()
            for (k,l) in box(i,j):
                s.difference_update( self.candidates[k][l] )
            if len(s)==1:
                d = s.pop()
                return(True,d)
            s = self.candidates[i][j].copy()
            for (k,l) in row(i,j):
                s.difference_update( self.candidates[k][l] )
            if len(s)==1:
                d=s.pop()
                return(True,d)
            s = self.candidates[i][j].copy()
            for (k,l) in column(i,j):
                s.difference_update( self.candidates[k][l] )
            if len(s)==1:
                d=s.pop()
                return(True,d)
        return(False,0)

    def not_in_region(self,i,j,d):
        '''Returns True if d is not in region of cell (i,j).'''
        for (k,l) in region(i,j):
            if self.grid[k][l] == d:
                return False
        else:
            return True

    def empty_cell(self):
        '''Returns the coordinates of the next empty cell.'''
        for (i,j) in product(range(9),repeat=2):
            if(self.grid[i][j]==0): 
                return (i,j)
        else:
            return

    def backtrack_solve(self):
        '''Bruteforce sudoku solving algorithm, will solve sudoku, might be very slow.'''
        coord = self.empty_cell()
        if not coord:
            return True
        else:
            (i,j) = coord
        for d in range(1,10):
            if self.not_in_region(i,j,d):
                self.grid[i][j] = d
                if self.backtrack_solve():
                    return True
                self.grid[i][j] = 0
        return False
    
    def solve(self,printgrid=False,bruteforce_fallback=True):
        '''Solves sudoku deterministically, and falls back to brute force if necessary'''
        while self.count > 0:
            (i,j) = self.stack.pop()
            d = self.grid[i][j]
            # Mise à jour des candidats : pour chaque case remplie, on met à
            # jour les candidats dans les cases appropriées. Si, lors de la mise
            # à jour, une case se retrouve avec un unique candidat, on met à
            # jour la grille.
            for (k,l) in region(i,j):
                if self.grid[k][l] == 0:
                    self.candidates[k][l].discard(d)
                    if len(self.candidates[k][l]) == 1:  # on met à jour
                        c = self.candidates[k][l].pop()
                        self.update_cell(k,l,c)
                    elif len(self.candidates[k][l]) == 0:
                        print("Pas de solution pour cette grille.")
                        return
            # Si stack est vide, l'algorithme n'a pas trouvé de case avec un
            # seul candidat. On commence par regarder s'il existe des cases dont
            # qui sont les seules à posséder un candidat dans leur
            # boîte/ligne/colonne respective.
            if self.stack==[]:
                for (i,j) in product(range(9),repeat=2):
                    (b, d) = self.only_in_region(i,j)
                    if b:
                        self.update_cell(i,j,d)
            # Si stack est toujours vide, l'algorithme ne peut pas résoudre
            # cette grille.
            if self.stack==[]:
                if bruteforce_fallback:
                    self.backtrack_solve()
                    return
                else:
                    print("Sudoku non résoluble par cet algorithme.")
                    return
        if printgrid:
            self.show()
        return


################################################################################
################################################################################

# pour tester :

def init_grid():
    global grid
    strgrid ="""
003020600
900305001
001806400
008102900
700000008
006708200
002609500
800203009
005010300
"""
    grid=[]
    for line in strgrid.split("\n"):
        if line!='':
            grid.append([int(d) for d in line])
    return

init_grid()
testsudo = sudoku(grid)

testsudo.show()
testsudo.init_candidates()
testsudo.solve(True)

########################################
# Pour ouvrir la grille n

n = 6

with open("p096_sudoku.txt","r") as raw_data:
    lines = raw_data.readlines()
raw_data.close()

grid = []
for line in lines[(n-1)*10+1:n*10]:
    grid.append([int(d) for d in line.strip()])

print_sudoku(grid)
solve_sudoku(grid,True)

################################################################################
# Pour résoudre toutes les grilles

t0 = time()

S = 0

with open("p096_sudoku.txt","r") as raw_data:
    for _ in range(50):
        print(raw_data.readline().strip())
        strgrid = [next(raw_data) for x in range(9)]
        grid = []
        for line in strgrid:
            grid.append([int(d) for d in line.strip()])
        s = sudoku(grid)
        s.init_candidates()
        s.solve()
        S += 100 * s.grid[0][0] + 10 * s.grid[0][1] + s.grid[0][2]
raw_data.close()

print(S)
print("Temps écoulé :",time()-t0,"s")
