import itertools as it

# décompte brute force des chemins dans Z/5Z possibles

def chemins(n):
    N = 5*n
    count = 0
    for c in it.product( [-1,1], repeat=N ):
        val = [0]*5
        s = 0
        for i in c:
            s = (s+i)%5
            val[s] += 1
        if s==0 and val == [n]*5:
            count += 1
    return(count)


################################################################################
# Méthode 1 : complexité O(n^5) ? temps d'exécution : 3 s
################################################################################
# L'idée est de voir le déplacement du robot comme une marche dans le graphe
# Z/5Z muni des arêtes (i,i+1). En effet, l'angle du robot est toujours un
# multiple de 2*pi/5 (s'il commence par 0), et il ajoute ou retranche 2*pi/5 à
# chaque étape. De plus, s'il passe de l'angle 2*k*pi/5 à 2*(k+1)*pi/5, il
# avance d'une unité dans la direction (2*k+1)*pi/5. Par incommensurabilité des
# cos(2*k*pi/5) et des sinus des mêmes angles, on en déduit que son trajet soit
# fermé, il faut et il suffit que
# 1) l'angle final soit 0,
# 2) il emprunte chaque arête exactement le même nombre de fois.
# On peut aller plus loin : comme le chemin est fermé, on peut montrer que le
# nombre de fois que le robot tourne à gauche (resp. à droite) est indépendant
# du sommet.
# 
# Il suffit donc de dénombrer les marches dans Z/5Z qui commencent en 0,
# finissent en 0, et passent exactement n fois par chaque valeur. On peut le
# calculer récursivement : soit a(x0,x1,x2,x3,x4 ; y) le nombre de chemins dans
# Z/5Z qui, partant de 0, passent xi fois par i, et termine en y. Alors, par
# exemple pour y = 3
#    a(x0, x1, x2, x3, x4 ; 3) = a(x0, x1, x2, x3 - 1, x4 ; 4)
#                              + a(x0, x1, x2, x3 - 1, x4 ; 2)
#
# On utilise une fonction récursive et de la mémoïsation pour stocker les
# valeurs intermédiaires.

from time import time
import functools

def cache(func):
    """Keep a cache of previous function calls"""
    @functools.wraps(func)
    def wrapper_cache(*args, **kwargs):
        cache_key = args + tuple(kwargs.items())
        if cache_key not in wrapper_cache.cache:
            wrapper_cache.cache[cache_key] = func(*args, **kwargs)
        return wrapper_cache.cache[cache_key]
    wrapper_cache.cache = dict()
    return wrapper_cache


@cache
def paths_count(target, x0, x1, x2, x3, x4):
    if x0<0 or x1<0 or x2<0 or x3<0 or x4<0:
        return(0)
    if x0+x1+x2+x3+x4 == 0:
        if target == 0: return(1)
        else: return(0)
    if target == 0:
        return( paths_count( (target+1)%5, x0-1, x1, x2, x3, x4) + paths_count( (target-1)%5, x0-1, x1, x2, x3, x4) )
    elif target == 1:
        return( paths_count( (target+1)%5, x0, x1-1, x2, x3, x4) + paths_count( (target-1)%5, x0, x1-1, x2, x3, x4) )
    elif target == 2:
        return( paths_count( (target+1)%5, x0, x1, x2-1, x3, x4) + paths_count( (target-1)%5, x0, x1, x2-1, x3, x4) )
    elif target == 3:
        return( paths_count( (target+1)%5, x0, x1, x2, x3-1, x4) + paths_count( (target-1)%5, x0, x1, x2, x3-1, x4) )
    elif target == 4:
        return( paths_count( (target+1)%5, x0, x1, x2, x3, x4-1) + paths_count( (target-1)%5, x0, x1, x2, x3, x4-1) )


t0 = time.time()
n=14
paths_count(0,n,n,n,n,n)
print(time.time()-t0)

################################################################################
# Méthode 2 : complexité O(n), temps d'exécution négligeable (30 us)
################################################################################
# On utilise le théorème BEST pour dénombrer les chemins eulériens d'un graphe
# orienté. On considère le multigraphe (non-orienté) de Z/5Z, avec, pour chaque
# i, n arêtes (i,i+1). D'après les propriétés explicitées plus haut, le choix
# d'une orientation du graphe revient au choix d'un entier k entre 0 et n, qui
# correspond au nombre d'arêtes partant vers la droite pour chaque sommet.
# 
# Soit un tel k. On calcule le nombre de chemins eulériens grâce au théorème
# BEST et au déterminant de la matrice de Kirchhoff :
#     e = (n-1)!5 * (n^4-3n^2(n-k)k + (n-k)^2k^2) = (n-1)!^5 * P(n,k)
# Chaque chemin eulérien passe n fois par 0, et peut donc être parcouru de n
# manières différentes au départ de 0. Par ailleurs, permuter les arêtes allant
# vers la droite d'un sommet donné n'a pas d'importance pour le robot, cela
# correspond à la même trajectoire. Il en va de même pour les arêtes allant vers
# la gauche. Ainsi, le nombre de circuits, pour un k donné, est
#    n * (n-1)!^5 * P(n,k) / (k!^5 * (n-k)!^5)
# et il suffit de sommer sur k pour trouver le nombre total de circuits.

import time
import functools

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter() 
        run_time = end_time - start_time 
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer

@timer
def BEST(n):
    # On pré-calcule les combinaisons
    combi = [1]
    for k in range(n):
        combi.append( (combi[-1]*(n-k))//(k+1) )
    # Définition du polynôme P
    def P(k):
        return( n**4 - 3*n**3 * k + 4*n**2 * k**2 - 2*n * k**3 + k**4 )
    return( sum( combi[k]**5 * P(k) for k in range(n+1)) // n**4 )
