
import numpy as np
import math
import itertools as it

########################################


# timer                          
from datetime import datetime

########################################

t0 = datetime.now()

# liste des nombres premiers < N - 1
def generate_primes(N):
    # crible d'Ératosthène
    primes=np.ones(N, bool)
    primes[0],primes[1]=False, False
    primes_list=[2]
    
    for i in range(2,N//2):
        primes[i*2]=False

    for p in range(3,math.ceil(math.sqrt(N))):
        if primes[p]:
            primes_list.append(p)
            i=p
            while i*p < N:
                primes[i*p]=False
                i+=2
        else:
            continue
            
    primes_list=[]
    for p,b in enumerate(primes):
        if b:
            primes_list.append(p)
    return(primes_list)

# test de divisibilité (et donc de primalité si primes est suffisante) 
def isprime(n,primes):
    for p in primes:
        if n%p==0:
            if n==p:
                return True
            else:
                return False
    return True


def range_minus(a,b,d):
    for i in range(a,b):
        if i!=d:
            yield(i)

def list2int(L):
    return int(''.join(str(e) for e in L))
    
# n = nombre de chiffres, d = chiffre répété, k = nombre de chiffres différents de d
# repeated_digits(3,1,1)=(110,112,113,114..., 101,121,...211,...911)
def repeated_digits(n,d,k):
    if d:
        if k>n:
            return
        if k==0:
            return( list2int([d]*n) )
        for idx in it.combinations(range(n),k):
            N = [d]*n
            if idx[0]:
                digits = it.product(range_minus(0,10,d),repeat=k)
            else:
                digits = it.product(range_minus(1,10,d),*it.repeat(range_minus(0,10,d),k-1))
            for di in digits:
                for i in range(k):
                    N[idx[i]]=di[i]               
                yield( list2int(N) )

    else:
        if k>n or k==0:
            return
        for idx in it.combinations(range(n),k):
            # combinations génère les combinaisons dans l'ordre lexicographique, donc on peut
            # arrêter dès que idx[0] n'est plus 0
            if idx[0]>0: 
                break
            N = [d]*n
            digits = it.product(range(1,10),repeat=k)
            for di in digits:
                for i in range(k):
                    N[idx[i]]=di[i]               
                yield( list2int(N) )



########################################

# n est le nombre de chiffres
# on étudie les nombres premiers p tq 1O^(n-1) < p < 10^n
# pour déterminer si p est premier ou pas, on génère tous les nombres
# premiers < sqrt(10^n)

n=10

if n%2==0:
    N=10**(n//2)
else:
    N= int(math.sqrt(10)*10**(n//2))

primes_list=generate_primes(N)


total = 0

print( "{:10s} {:10s} {:10s} {:20s}".format("d","M("+str(n)+",d)","N("+str(n)+",d)","S("+str(n)+",d)" ) )
print( "{:-^50}".format("") )

d=0
k=1
while (d<10):
    Snd=0
    Nnd=0
    stop=False
    for number in repeated_digits(n,d,k):
        if isprime(number,primes_list):
            Snd+=number
            Nnd+=1
            stop=True
    if stop:
        print( "{:<10d} {:<10d} {:<10d} {:<20d}".format(d, n-k, Nnd, Snd) )
        k=0
        d+=1
        total += Snd
    else:
        k+=1

print( "{:-^50}".format("") )
print( "{:32s} {:<20d}".format("total = ", total) )

# réponse : total = 612407567715

########################################

print("-- Temps écoulé : ", datetime.now()-t0)

