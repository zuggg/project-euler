
from time import time

# Calcul naïf

def P(n):
    v = [d for d in range(1,n+1)]
    rev = False
    while len(v) > 1:
        if rev:
            v = v[len(v)%2::2]
        else:
            v = v[1::2]
        rev = not rev
    return(v[0])

f = [P(2**(2*k)) for k in range(1,10)]
g = [P(k) for k in range(1,100)]

# On remarque que pour tout n, P(2n) = 2n - 2(P(n)-1), et P(2n+1) = P(2n), et
# donc
#    P(4n)   = P(4n+1) = 4P(n) - 2 
#    P(4n+2) = P(4n+3) = 4P(n)

def PP(n):
    if n==1:
        return(1)
    elif n==2 or n==3:
        return(2)
    elif n%4 < 2:
        return(4*PP(n//4) - 2)
    else:
        return(4*PP(n//4))

# On peut calculer la somme naïvement...
    
def SS(n):
    return( sum( PP(k) for k in range(1,n+1)) )

# ...mais c'est plus intéressant d'utiliser la propriété de récurrence
#    P(4n) + P(4n+1) + P(4n+2) + P(4n+3) = 16*P(n) - 4
# d'où
#    S(4n+3) = sum P(k) pour k entre 1 et 4n + 3
#            = 5 + sum P(k) pour k entre 4 et 4n + 3
#            = 5 + 16*S(n) - 4*n

def S(n):
    if n==1:
        return(1)
    if n==2:
        return(3)
    if n==3:
        return(5)
    if n%4==3:
        k = n//4
        return(5 + 16*S(k) - 4*k)
    else:
        return( PP(n) + S(n-1) )

# On pourrait encore améliorer l'algorithme, mais il est déjà plutôt rapide pour
# ce problème (400 us).

t0 = time()
n, mod = 10**18, 987654321

print(S(n)%mod, time()-t0)
