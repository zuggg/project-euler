from primes import generate_primes
from math import sqrt, log, floor
from time import time

def dicho_search(L,f,k):
    '''Returns the first element from sorted list L such that its image through f is greater than k'''
    a = 0
    b = len(L)-1
    if f(L[a],a) > k: return a
    if f(L[b],b) <= k : return
    while b-a > 1:
        c = (a+b)//2
        if f(L[c],c) > k: b = c
        else: a = c
    return( b, L[b] ) 

def f(el,idx):
    return( 2*(2*idx+1)*el )

# Comme dans le problème 120, 
#    (pn - 1)^n + (pn + 1)^n = 2*n*pn  [ pn^2 ]
# lorsque n est impair, et 2 sinon. De plus, on sait que
#    pn > n*( log(n) + log(log(n)) - 1),
# et donc si 2*n^2 * log(n) > N (et n > 15), alors 2*n*pn > N.

t0 = time()

target = 10**12

# Le but est de calculer le moins de nombres premiers possible. Pour cela, on
# commence par trouver une borne supérieure sur n, puis sur pn.

n = int(sqrt(target/2))
i = 1

while True:
    m = (n*i)//(i+1)
    if 2 * m**2 * log(m) > target:
        n = m
        i += 1
    else:
        break

# primes.py:
if 0<n and n<13: # borne sup
    N =38
elif n<8062:
    N = floor(n*(log(n)+log(log(n)) -1 + 1.8*log(log(n))/log(n)))
elif n<15985:
    N = floor( n*(log(n)+log(log(n)) - 0.9385 ))
else:
    N = floor( n*(log(n)+log(log(n)) - 0.9427 ))

plist = generate_primes(N)[::2]

print( 2*dicho_search( plist, f, target )[0] +1, "\n", time()-t0 ) 
# attention, car 2*n*pn > pn**2 pour n=3 par exemple. Ce n'est pas un problème
# lorsque n est grand.


