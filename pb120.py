
from time import time

# On développe simplement
#    (a-1)^n + (a+1)^n
# pour obtenir le reste modulo a^2, qui s'écrit :
#    a*n*( 1 + (-1)^(n-1) ) + ( 1 + (-1)^n ) 
# Alors le reste maximum est donné par
#    max a*(2*k+1)*2, k dans N.
# On peut se restreindre aux k entre 0 et a-1.

def rmax(a):
    r = 0
    for k in range(a):
        r = max(r, (4*k+2)%a)
    return(a*r)

t0=time()
print( sum( rmax(a) for a in range(3,1001)), "\n", time()-t0)


# On peut aller un tout petit peu plus loin, en remarquant que
#    rmax = a*(a-1)  si a est impair
#           a*(a-2)  si a est pair.
# et donc
#    sum rmax(a) = sum a*(a - 2 + a%2) = P(amax) - P(amin),
# avec
#    P(n) = n*(n+1)*(n-1)/3 + n//2*(n//2+1)

def rmax_sum(amin,amax):
    def P(a):
        return (a*(a+1)*(a-1))//3 - a//2*(a//2+1)
    return P(amax)-P(amin-1)

t0 = time()
print( rmax_sum(3,10**50), time() - t0)
