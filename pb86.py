
from time import time
from math import sqrt

# Le chemin le plus court pour aller d'un sommet d'un pavé au sommet opposé est
# de longueur L^2 = x^2 + (y+z)^2 si x ≥ y ≥ z. Les pavés pour lesquels L est
# entier correspondent donc aux triplets pythagoriciens. 
# En particulier, si 2sqrt(M) ≥ p > q, avec pgcd(p,q)=1 et pq = 0 [mod 2], alors
# (x, y+z) = (p^2-q^2, 2pq) ou (x, y+z) = (2pq, p^2-q^2) selon que p^2-q^2 > 2pq
# ou l'inverse, correspondent à des solutions du problème.

def gcd(a,b):
    if b==0:
        return(a)
    else:
        return(gcd(b,a%b))    


def solutions_below(M):
    candidates=[]                   # de la forme [x,y+z] 
    for p in range(2,int(sqrt(3*M))):
        for q in range(1, min(p,int(sqrt(M)))):
            if gcd(p,q)==1 and (p*q)%2 ==0:
                a, b = 2*p*q, p**2-q**2
                if ( a < 2*b) and ( a <= 2*M and b <= M):
                    candidates.append([b,a])
                if ( b < 2*a) and ( b <= 2*M and a <= M):
                    candidates.append([a,b])

    count = 0
    for [a,b] in candidates:
        if a>b:
            m=1
            while m*a<=M:       # on compte tous les multiples
                count += (m*b)//2
                m+=1
        else:
            m=1
            while m*a<=M:
                count += m*a+1-(m*b+1)//2
                m+=1
    return(count)

# recherche de la solution par dichotomie
def dich(N):
    a=100
    b=1000
    
    while (solutions_below(b)<=N):
        a, b = b, 2*b

    while (b-a > 1):
        c = (a+b)//2
        if (solutions_below(c)<=N):
            a = c
        else:
            b = c
    return(b)

# Cette approche pourrait être grandement optimisée, car
# 1/ la génération des paires d'entiers premiers entre eux est particulièrement
# lente, et
# 2/ avec la recherche par dichotomie, on génère beaucoup de fois les mêmes
# listes.
# Cela étant, l'algorithme est suffisament rapide pour N=10^6 (44 ms).

t0=time()
N=10**6
print(dich(N))
print(time()-t0)

