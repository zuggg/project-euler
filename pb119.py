
from math import log, log10, ceil
from time import time

def sod(n):
    return( sum( int(d) for d in str(n) ))

def searchforanswers(Nmax):
    k = ceil(log10(Nmax) /( log10(9) + log10(log10(Nmax) + 1)))
    nmax = int(Nmax**(1/k))
    ans = []
    for n in range(3,nmax):
        k=2
        m = n**2
        while m < Nmax:
            if sod(m) == n:
                ans.append([m,k])
            m *= n
            k += 1
    return(sorted(ans))


t0 = time()
print(searchforanswers(10**20)[n-1], time()-t0)
