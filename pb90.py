# Pour chaque dé, on a 6 parmi 10 = 210 choix. Par conséquent, le nombres
# d'arrangements distincts est 210*211/2 = 22155. C'est suffisament peu pour une
# approche naïve.

import itertools as it
from time import time

def can_form(l,d1,d2):
    if l==[]:
        return True
    a = l[0]//10
    b = l[0]%10
    if (a in d1 and b in d2) or (a in d2 and b in d1):
        return can_form(l[1:],d1,d2)
    else:
        return False

def extended_digits(d):
    if (6 in d) and (9 not in d):
        return(d+(9,))
    elif (9 in d) and (6 not in d):
        return(d+(6,))
    else:
        return(d)

t0 = time()
    
digits = [i for i in range(10)]
squares = [i**2 for i in range(1,10)]

count=0

for d1,d2 in it.combinations_with_replacement(it.combinations(digits,6),2):
    if can_form(squares,extended_digits(d1),extended_digits(d2)):
        count+=1

print(count,"\nTemps écoulé :",time()-t0)
