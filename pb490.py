# timer                          
from datetime import datetime

t0 = datetime.now()

########################################


# On peut construire une formule qui donne f(n) par récurrence. Pour cela, on
# définit les quantités g(n) et h(n) juste après. Le symbole ○ désigne la
# position initiale de la grenouille, les cases □ sont les positions non
# visitées, et les cases ■ sont les positions déjà visitées.

# f(n) = # manières de remplir ○□□  ... □ en finissant sur la dernière case
# g(n) = # manières de remplir ○■□  ... □ en finissant sur la dernière case
# h(n) = # manières de remplir ○■■□ ... □ en finissant sur la dernière case

# On voit facilement que h(n)=f(n-3).

# On raisonne alors comme suit : pour un bon remplissage de n cases, il existe
# des étapes auxquelles les cases visitées (y compris la position actuelle) sont
# consécutives, par exemple ■ ... ■○■□ ... □.  Soit k la première étape (>1)
# telle qu'on soit dans cette configuration. La position actuelle est forcément
# en k, k-1 ou k-2.

# On définit alors F(k), G(k) et H(k) les quantités suivantes :

# F(k) = # manières de remplir ○□□ ... □ en finissant sur la dernière case, et
# sans jamais n'avoir visité que des cases consécutives
# G(k) = # manières de remplir ○□□ ... □ en finissant sur l'avant dernière case,
# et sans jamais n'avoir visité que des cases consécutives
# H(k) = # manières de remplir ○□□ ... □ en finissant sur l'avant-avant dernière
# case, et sans jamais n'avoir visité que des cases consécutives

# Ces quantités sont plus faciles à dénombrer, on peut étudier les cas k<10 pour
# se convaincre d'une formule simple. La formule qui donne f est alors

# f[n] = f[n-1]
#         + sum( G(k+2)*g[n-k] for k in range(1,n-2) )
#         + sum(  H(k)*f[n-k]  for k in range(4,n+1) )  

# En définissant F2, G2 et H2 de la même manière que F, G et H mais en partant
# de la configuration ○■□ ... □, on peut déduire une formule pour g:

# g[n] = f[n-2]
#         + sum( G2(k+2)*g[n-k] for k in range(2,n-2) )
#         + sum(  H2(k)*f[n-k]  for k in range(5,n+1) )


def G(k):
    if k<3:
        return(0)
    elif (k==3 or k%3!=0):
        return(1)
    else:
        return(2)

def H(k):
    if k<4:
        return(0)
    elif (k>5 and k%3!=1):
        return(1)
    else:
        return(2)


def G2(k):
    if (k<4 or k%3==2):
        return(0)
    else:
        return(1)

def H2(k):
    if (k<5 or k%3==0):
        return(0)
    else:
        return(1)


# Calcul de f(n) pour n < 1000
    
f=[1]*4
g=[1]*4

for n in range(4,1000):
    f.append( f[n-1] + sum( G(k+2)*g[n-k] for k in range(1,n-2) ) + sum( H(k)*f[n-k] for k in range(4,n))  )
    g.append( f[n-2] + sum( G2(k+2)*g[n-k] for k in range(2,n-2) ) + sum( H2(k)*f[n-k] for k in range(5,n)) )

# Malheureusement cette méthode est très lente, presque une seconde pour
# calculer f[1000]. 

# Un argument algébrique permet de montrer que f(n) est récurrente d'ordre au
# plus 29, en assimilant les trajectoires admissibles à des mots dans l'alphabet
# formé des configurations possibles pour trois cases consécutives, qui est de
# cardinal 29.
# Les trajectoires admissibles de taille n sont alors décrites par des mots de
# longueur n-2 dans un graphe à 29 sommets.

# On peut faire un peu de calcul symbolique avec Python. Pour trouver une
# relation de récurrence, on regarde le déterminant d'une matrice glissante
# comme ci-dessous.

from sympy import Matrix

order_f=1
while True:
    for N in range(1,30):
        if Matrix([ f[i:i+order_f+1] for i in range(N,N+order_f+1) ] ).det() != 0:
            order_f+=1
            break
    else:
        print("f(n) semble être récurrente d'ordre",order_f)
        break


# Pour trouver une relation de liaison entre les colonnes, on peut calculer la
# matrice échelonnée réduite. Les coefficients apparaissent dans la dernière
# colonne.

m=Matrix([ f[i:i+order_f+1] for i in range(1,order_f+2) ] )

A,_=m.rref()

# On vérifie que la suite ainsi définie par récurrence avec ces coefficients
# correspond bien à f.

coeff=list(A[:-1,-1])+[-1]

# f2=f[:size]
# for n in range(1000):
#     f2.append( sum( f2[-size+i+1]*coeff[i] for i in range(size-1) ) )
# print(f2[:len(f)]==f)
# >>> True

################################################################################
# Idée 1 : on utilise la relation de récurrence pour déterminer une expression
# exacte de la suite f(n) comme une somme de puissances. La suite f(n)^3 s'écrit
# alors comme une somme de puissances, avec au plus 3 parmi 10, c'est-à-dire 120
# termes. On finit par calculer S(n) comme une somme de sommes de suites
# géométriques.
########################################
# Problème : le polynôme caractéristique n'est pas résoluble de manière exacte,
# et on doit donc travailler avec une précision bien supérieure avec ce qui est
# possible. Je ne sais pas si on peut contourner ce problème.


# On peut maintenant déterminer une expression exacte de f(n) grâce aux racines
# du polynôme coeff. Pour déterminer les racines d'un polynôme, numpy utilise la
# matrice compagnon et ses valeurs propres, donc autant le faire directement.
# Le polynôme caractéristique de f(n) est P=A[:-1,1:].charpoly()

# B=A[:-1,1:]
# B.eigenvals()

# p=B.charpoly()
# factor(p)

# Apparemment, sympy utilise des méthodes exactes pour calculer les valeurs
# propres, et ce n'est malheureusement pas possible dans notre cas. On peut
# utiliser numpy, mais il faut faire attention car sympy a son propre type
# d'entiers et empêche numpy de les convertir en float, ce qui casse roots().

# import numpy as np              
# r=np.roots([-1]+coeff[::-1])

################################################################################
# Idée 2 : f(n)^3 est une somme de 120 (au plus) puissances, et donc S(n) est
# récurrente d'ordre au plus 121. En effet, f(n)^3=S(n)-S(n-1), et on peut donc
# faire apparaître une relation de récurrence d'ordre k+1 pour S(n) à partir
# d'une relation de récurrence d'ordre k pour f(n)^3. Pour chercher cette
# relation, il nous faut donc calculer les 241 premiers termes.
########################################
# Problème : sympy est très très lent, le calcul ne finit pas !

#import scipy as sp
# import numpy as np
# from scipy.linalg import null_space

# modulo=10**9
# S=[0]
# for i in range(250):
#     S.append( S[-1]+f[i+1]**3 )

# size=15

# M = np.matrix([ S[i:i+size] for i in range(1,size+1) ], dtype=int)

# v=null_space(M)


# size=122
# M=Matrix([ S[i:i+size] for i in range(1,size+1) ] )

# t0=datetime.now()
# print(M.nullspace())
# print(datetime.now()-t0)

# B,_=M.rref()

# print(B)


# ########################################

# from sympy import *

# # projection de a sur u
# def proj(a,u):
#     return( Rational(a.dot(u))/Rational(u.dot(u))*u )

# def projf(a,u):
#     return( a.dot(u)/u.dot(u) * u )


# # GS utilise le procédé d'orthogonalisation de Gram-Schmidt (sans la
# # normalisation, pour rester dans Q), et renvoit une matrice Q dont les colonnes
# # sont orthogonales.
# def GS(A):
#     n = A.shape[0] # dimension
#     Q = A.col(0)
#     for j in range(1,n):
#         u = a = A.col(j)
#         for i in range(j):
#             u -= projf(a,Q.col(i))
#         Q = Q.row_join(u)
#     return(Q.col(-1))


# test=30
# GS(M[:30,:30])


################################################################################

# import numpy as np

# def proj_np(a,u):
#     return( np.dot(a,u)/np.dot(u,u)*u )

# def GS_np(A):
#     n = len(A) # dimension
#     Q = np.zeros([n,n],dtype=float)
#     Q[:,0] = A[:,0]
#     for j in range(1,n):
#         u = a = A[:,j]
#         for i in range(j):
#             u -= proj_np(a,Q[:,i])
#         Q[:,j] = u
#     return(Q)

# sizem=9
# m=np.array([ f[i:i+sizem] for i in range(1,sizem+1) ] ,dtype=float)

################################################################################
# Idée 3 : au lieu de chercher une relation de liaison dans la matrice 122x122
# de termes de S[n], on détermine le polynôme caractéristique de f[n]^3
# directement grâce aux racines de celui de f[n]. Python est trop lent avec le
# calcul matriciel, mais rapide pour cette approche numérique.  Le type
# numpy.complex128 n'est pas assez précis pour cette opération car les
# coefficients sont grands, de l'ordre de 10^14, on a donc recourt à mpmath

# Les coefficients du polynôme annulateur sont [-1,-1,0,1,1,2,-1,2,-1]

from mpmath import *
import numpy as np
mp.dps=50

# coeff=[-1,-1,0,1,1,2,-1,2,-1]   
coeff = [int(i) for i in coeff]
r=polyroots(coeff[::-1]) # mpmath polynomial root finder

# Les racines du polynôme annulateur de f(n)^3 sont donc données par :
r3 = []
for i in range(order_f):
    for j in range(i,order_f):
        for k in range(j,order_f):
            r3.append( r[i]*r[j]*r[k] )

# On calcul les coefficients du polynôme annulateur de f(n)^3 grâce aux
# polynômes symétriques élémentaires.

order_f3 = len(r3)

E = matrix(order_f3+1) # mpmath matrix
E[:,0] = ones(order_f3+1,1)

for i in range(1,order_f3+1):
    for j in range(1,i+1):
        E[i,j] = E[i-1,j] + r3[i-1]*E[i-1,j-1]

# On vérifie que les erreurs d'approximation ne sont pas trop grandes
# for i in range(l+1):
#     if ( abs( round(E[l,i].real) - E[l,i]) > 0.01 ):
#         print(i, "oups", E[l,i])

coeff3 = [ (1-2*(i%2))*int(round(E[order_f3,i].real)) for i in range(order_f3+1) ]

# On a une relation de la forme sum( c_k*f[n+k]**3 = 0 for k in range(l)), on
# peut donc en déduire une relation pour S[n], grâce à f[n]^3=S[n]-S[n-1].
coeff_S = coeff3+[0]

for i in range(len(coeff3)):
    coeff_S[i+1] -= coeff3[i]

order_S = order_f3+1

# Vérification
S=[1]
for i in range(order_S - 1):
    S.append( S[-1]+f[i+2]**3 )

# S2=S[:order_S]
# for n in range(200):
#     S2.append( sum( -S2[-(i+1)]*coeff_S[i+1] for i in range(order_S) ) )
# print(S2==S[:len(S2)])
    
# Matrice de récurrence dans Z/nZ:

modulo=10**9

MS = np.zeros([order_S,order_S], dtype=object)
MS[:-1,1:]= np.eye(order_S-1, dtype=object)
for i in range(order_S):
    MS[-1,i] = (- coeff_S[-(i+1)])%modulo

# La multiplication matricielle à la main est _beaucoup_ plus lente que la
# multiplication matricielle de numpy (@), même avec des dtype=object plutôt que
# des int. 

# def mult_mod(A,B,modulo):
#     n = len(A)
#     C = np.zeros( [n,n] , dtype=int)
#     for i in range(n):
#         for j in range(n):
#             for k in range(n):
#                 C[i,j] = (C[i,j]+A[i,k]*B[k,j])%modulo
#     return(C)

# def fastp_mod(M,p,modulo):
#     if p==1:
#         return( M )
#     elif p%2 == 0:
#         return( fastp_mod( mult_mod(M,M,modulo), p//2, modulo) )
#     else:
#         return( mult_mod( M,  fastp_mod( M, p-1, modulo), modulo ) )


def fastp_mod(M,p,modulo):
    if p==1:
        return( M )
    elif p%2 == 0:
        return( fastp_mod( (M@M)%modulo, p//2, modulo) )
    else:
        return( (M @ fastp_mod( M, p-1, modulo))%modulo )


# def fastp(M,p):
#     if p==0:
#         return( np.eye(len(M), dtype=object) )
#     elif p%2 == 0:
#         return( fastp( M@M, p//2) )
#     else:
#         return( M @ fastp(M,p-1)) 
    
# S=[1]
# for i in range(1,500):
#     S.append( S[-1]+f[i+1]**3 )

# On vérifie que ça marche pour les premières valeurs.

# for p in range(1,50):
#     if(np.dot(  (fastp_mod(MS,p,modulo))[-1,:], S[:l+1] )%modulo - S[120+p]%modulo):
#         print(p)
#         break

def fast_S(n):
    p=n-121
    print("S["+str(n)+"] =",  np.dot(  (fastp_mod(MS,p,modulo))[-1,:], S )%modulo,"(mod "+str(modulo)+")") 


# fast_S(1000)                    
fast_S(10**14)      # answer : 777577686


########################################

print("-- Temps écoulé : ", datetime.now()-t0)
