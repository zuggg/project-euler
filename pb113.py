# timer                          
from datetime import datetime

t0 = datetime.now()

########################################


# on calcule le nombre de nombres monotones (non-bouncy) entre 1 et 10^E, il
# suffit donc de compter les suites croissantes de [1,9]^n pour n entre 1 et E
# et les suites décroissantes de [1,9]*[0,9]^(n-1) pour n entre 1 et E

# #{suites croissantes de [1,k]^n} = n parmi (n-1+k) et donc
# #{suites croissantes de \bigcup_{n=0}^N [1,k]^n} = k parmi (N+k)

# de la même manière,
# #{suites décroissantes de [0,k]^n} = k parmi (n+k) et
# #{suites décroissantes de \bigcup_{n=0}^N [0,k]^n} = (k+1) parmi (N+k+1)

E = 100

def bin(n,k):
    if k==0:
        return(1)
    else:
        return( (n*bin(n-1,k-1))//k )

# il faut enlever 102 au total car on a compté le nombre 0 102 fois, et on
# enlève les nombres croissants et décroissants
    
print( bin(E+9,9) -1 + bin(E+10,10) -1 -E -9*E)

# answer : S = 51161058134250

########################################

print("-- Temps écoulé : ", datetime.now()-t0)
