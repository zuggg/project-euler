
# Une approche bruteforce fonctionne en moins d'une minute.

# timer                          
from datetime import datetime

################################################################################
# On préfère utiliser une approximation de la suite de Fibonacci.
# Le type flottant est limité à 10**300, et cela ne suffit pas dans ce problème
# on peut utiliser la librairie decimal pour y palier.
# On a besoin par ailleurs d'une précision suffisament grande
from decimal import * 


# pandigit renvoit True si le nombre est égal à un anagramme de 123456789
def pandigit(n):
    return sorted(n) == [str(i) for i in range(1,10)]

def fib_approx(n):
    alpha=(Decimal(5).sqrt()+1)/2
    return( alpha**n/Decimal(5).sqrt() )

def decimal_approx():
    startTime = datetime.now()
    
    a=1
    b=1
    n=2

    while( True ):
        a,b = b, (a+b)%1000000000
        n += 1
        if pandigit(str(b)):
            if pandigit(str(fib_approx(n)).replace(".","")[0:9]):
                print("Le",n,"ième nombre de Fibonacci commence par",str(fib_approx(n)).replace(".","")[0:9],"et finit par",b)
                break
            
    print("-- Temps écoulé :", datetime.now()-startTime)

########################################
# On peut aussi procéder sans utiliser la librairie decimal, en générant les
# chiffres dominant itérativement. Bien sûr, l'erreur s'accumule, mais on peut
# garantir une précision à 9 chiffres significatifs tant qu'on ne dépasse pas un
# certain nombre d'étapes (10^(N-9) itérations)

def generateFibonacciLSD():
    yield 0; yield 1; yield 1
    f_2, f_1 = 1, 1
    while True:
        f = (f_2 + f_1) % 1000000000
        yield f
        f_2, f_1 = f_1, f

def generateFibonacciMSD(N):
    yield 0; yield 1; yield 1
    f_2, f_1 = 1, 1
    while True:
        f = f_2 + f_1
        if f >= 10**N:
            f = int(str(f)[0:N])
            f_1 = int(str(f_1)[0:N-1])
        yield f
        f_2, f_1 = f_1, f


def iterative_approx():
    startTime = datetime.now()
    N=20
    TARGET = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    for ii, (msd, lsd) in enumerate(zip(generateFibonacciMSD(N), generateFibonacciLSD())):
        if sorted(str(lsd)) == TARGET and sorted(str(msd)[0:9]) == TARGET:
            print('Réponse %d' % ii)
            break
    print("-- Temps écoulé :", datetime.now()-startTime)

# réponse : n=329468

################################################################################


method = int(input("Méthode (1: approximation décimale, 2: approximation itérative) : " ))

if (method==1):
    decimal_approx()
elif method==2:
    iterative_approx()
