
from time import time

################################################################################
# Méthode 1 : temps d'exécution 1.0652856826782227 s
################################################################################
# Une approche en bruteforce est complètement hors de question, puisque le
# nombre de configurations possibles = 2^(emplacements de mur)
#                                    = 2^(2*n*m-n-m)
# c'est-à-dire, dans notre cas, 2^99400 ~ 10^30000.
#
# Le problème revient à dénombrer les arbres couvrants du graphe (V,E), avec V =
# {1,...,n}×{1,...,m} et E = {((i,j),(i',j')) tq |i-i'|+|j-j'|=1}.  Pour cela,
# on peut utiliser le théorème de Kirchhoff, qui dit que ce nombre est égal (au
# signe près) à un (n'importe lequel) cofacteur de L, la matrice laplacienne du
# graphe.  (cf https://en.wikipedia.org/wiki/Kirchhoff%27s_theorem)
#
# La matrice laplacienne du graphe G = ( V={v_i}, E={(v_i,v_j) } est définie de
# la manière suivante :
#    l_{i,j} = deg(v_i) si i==j
#              -1       si i!=j et (v_i,v_j) est dans E
#              0        sinon
#
# En supposant que n < m, et en numérotant les sommets de la manière suivante :
#
#  1 n+1   ⋅⋅⋅  (m-1)n+1
#  2 n+2        .
#  .  .         .
#  :  :         ⋅
#  n 2n    ⋅⋅⋅  mn
#
# la matrice laplacienne s'écrit
#  –––––––––––––––+–––––––––––––––+–––––––––––––––+–––⋅⋅⋅
# | 2 -1          |               |               |
# |-1  3          |               |               | 
# |      ⋅        |     -I_n      |       0       |
# |        ⋅      |               |               |
# |          3 -1 |               |               |               
# |         -1  2 |               |               |
#  ———————————————+———————————————+———————————————+–––⋅⋅⋅
# |               | 3 -1          |               |
# |               |-1  4          |               |
# |    -I_n       |      ⋅        |      -I_n     |
# |               |        ⋅      |               |
# |               |          4 -1 |               |
# |               |         -1  3 |               |
#  ———————————————+———————————————+———————————————+–––⋅⋅⋅
# |               |               |               |
# :               :               :               :         
#
# Il suffit donc de calculer un cofacteur.

# Fonction écrite par Martin Reiner sur le forum du problème 380 :
import numpy as np
import scipy.sparse
from scipy.sparse import csc_matrix
from scipy.sparse import linalg
import math

def f(i, j, n):
    return i+n*j

def corner(i, j, n, m):
    return (i == 0 or i == n - 1) and (j == 0 or j == m - 1)

def border(i, j, n, m):
    return (i== 0 or i == n - 1) or (j == 0 or j == m - 1)

def C(n,m):
    # on commence par construire la matrice
    data=[]
    row=[]
    col=[]
    for i in range(n):
        for j in range(m):
            r = f(i, j, n)
            if r == n*m-1:
                continue
            if corner(i, j, n, m):
                data.append(2.0)
            elif border(i, j, n, m):
                data.append(3.0)
            else:
                data.append(4.0)
            row.append(r)
            col.append(r)

            if i > 0:
                row.append(r)
                col.append(f(i - 1, j, n))
                data.append(-1.0)
            if i < n - 1 and f(i + 1, j, n) < n * m - 1:
                row.append(r)
                col.append(f(i + 1, j, n))
                data.append(-1.0)
            if j > 0:
                row.append(r)
                col.append(f(i, j - 1, n))
                data.append(-1.0)
            if j < m - 1 and f(i, j + 1, n) < n * m - 1:
                row.append(r)
                col.append(f(i, j + 1, n))
                data.append(-1.0)
            
    a = csc_matrix((data, (row, col)), shape=(n*m - 1, n*m- 1))
    lu = scipy.sparse.linalg.splu(a)
    x = np.log10(lu.U.diagonal()).sum()
    x_frac, x_int = math.modf(x)

    print("{:.4f}e{}".format(10.0 ** x_frac, round(x_int)))
    return

t0=time()
n, m = 100, 500
C(n,m)
print("Temps écoulé :",time()-t0,"s")

################################################################################
# Méthode 2 : temps d'exécution 0.2880840301513672 s
#################################################################################
# On peut aussi déterminer les valeurs propres de la matrice laplacienne, le
# nombre d'arbres couvrants s'exprimant comme le produit des valeurs propres non
# nulles, divisé par le nombre de sommets, m*n. Le graphe considéré est
# particulier, car il est en fait le produit cartésien des chemins C(n) et C(m)
# ( C_n : 1 – 2 – ⋅⋅⋅ – n )
# La matrice laplacienne de la grille M(n,m) = C(n)×C(m), à l'instar de sa
# matrice d'adjacence, s'écrit comme le symétrisé du produit de Kronecker des
# matrics laplaciennes de C(n) et C(m),
#    L(M(n,m)) = I_m ⊗ L(C(n)) + L(C(m)) ⊗ I_n.
# Comme de plus ces termes commutent, les valeurs propres de L(M(n,m)) sont les
# sommes de valeurs propres de L(C(n)) et L(C(m)), qu'on détermine grâce à leur
# polynôme caractéristique.
#
# Notons P_n = det( X*I_n - L(C(n)) ). Alors P_n vérifie la relation de
# récurrence
#    P_{n+2} = (X-2)*P_{n+1} - P_n,
# et par conséquent s'exprime comme une combinaison linéaire de polynômes de
# Tchebychev. En l'ocurrence,
#    P_{n+1}(X) = X*U_n(X/2 - 1),
# où U_n est le polynôme de Tchebychev de seconde espèce de degré n, et vérifie
#    P_n(cos(t)) = sin((n+1)*t) / sin(t).
# Par conséquent,
#    spectre(L(C(n))) = {0} U { 2 + 2*cos(k*pi/(n+1)), k entre 1 et n}.


def spanning_trees(n,m):
    from mpmath import cos, pi
    #from mpmath import mp
    #mp.dps = 100
    sp1 = [ 2+2*cos(k*pi/n) for k in range (1,n)]
    sp2 = [ 2+2*cos(k*pi/m) for k in range (1,m)]

    res = 1/(n*m)
    for v in sp1:
        for w in sp2:
            if (v,w)==(0,0): continue
            res *= v+w
    return(res)

t0 = time()
print(spanning_trees(500,100))
print("-- Temps écoulé :", time()-t0)

# résultat = 6.320236506992615984727471042307112350655772582190397033619308007209001742627606719770953782369041999e+25093
# 6.3202e25093


