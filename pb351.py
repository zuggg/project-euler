########################################

from time import time
#from math import sqrt, ceil
from primes import generate_primes

########################################

# F(n) = #{ (x,y), 0 < x/y < 1 }
def F(n):
    return( (n*(n-1))//2 )


# R(n) = #{ (x,y), 0 < x/y < 1, pgcd(x,y)=1 }
def R(N,index=0):
    count = F(N)
    while (index < len(primes) and primes[index]*2 <= N):
        count -= R(N//primes[index], index+1)
        index += 1
    return(count)


################################################################################
# Méthode 1 : complexité O(N), temps d'exécution 30s 
################################################################################
# On commence par remarquer qu'on peut se ramener à une figure dans Z^2 grâce à
# la transformation suivante :
#
#   ⋅ ⋅           ⋅ ⋅
#  ⋅ ⋅ ⋅   __\  ⋅ ⋅ ⋅
#  ⋅ ⋅ ⋅     /  ⋅ ⋅ ⋅
#   ⋅ ⋅         ⋅ ⋅
#
# Comme la transformation est linéaire, elle conserve la colinéarité, et en
# particulier, compter le nombre de points cachés est inchangé. De plus, par
# symétrie de l'hexagone, il suffit de compter les points cachés dans l'ensemble
# {(x,y) 0<= x < y <= N }, puis de multiplier par 6.
# Or, on sait compter le nombre de points visibles dans cet ensemble : c'est
# exactement les couples (x,y) qui vérifient en plus pgcd(x,y)=1, et c'est le
# sujet du problème 73, qu'on peut résoudre grâce à une inversion de Moebius
# pour obtenir un algorithme linéaire.
# Ainsi, #{points cachés dans l'hexagone de côté N}
#      = 6 ( N*(N+1)/2 - #{(x,y), 0 <= x < y <= N, pgcd(x,y)=1} )
#      = 3*N*(N+1) - 6*( 1 + #{(x,y), 0 < x < y <= N, pgcd(x,y)=1} )
#      = 3*N*(N+1) - 6 - 6*R(N)


t0 = time()

N = 10**8
primes = generate_primes(N//2+2)

print( 3*N*(N+1) - 6 - 6*R(N)  )

print("-- Temps écoulé : ", time()-t0,"s")

# 11762187201804552

################################################################################
# Méthode 2 : complexité O(N^3/4), temps d'exécution 1.3 s
################################################################################
# Comme dans le problème 73, on a vu que
#   F(N) = sum R(N//d) pour d entre 1 et N,
# et donc
#   R(N) = F(N) - sum R(N//d) pour d entre 2 et N.
# Or, lorsque d décrit [2,N], N//d décrit un ensemble beaucoup plus petit. À
# partir de d >= sqrt(N), les valeurs de N//d se répètent. En particulier, si
# 1 <= z < sqrt(N), #{d, N//d = z} = N//z - N//(z+1).
# On peut alors écrire, en posant L = floor(sqrt(N))
#   R(N) = F(N) - sum R(N//d) pour d entre 2 et L
#               - sum (N//z - N//(z+1))*R(z) pour z entre 1 et N//(L+1)
# Comme de plus (N//d)//d' = N//(dd'), il suffit, pour calculer R(N), de
# calculer R(d) et R(N//d) pour d entre 2 et sqrt(N).

from math import sqrt, floor

def R2(N):
    L = floor(sqrt(N))          
    r = [0]*(L+1)               # on stocke les valeurs de R(n) pour n<=sqrt(N) 

    for n in range(1,L+1):
        r[n] = (n*(n-1))//2
        l = floor(sqrt(n))       
        for d in range(2,l+1):  # première somme
            r[n] -= r[n//d]
            
        for z in range(1,n//(l+1)+1):  # deuxième somme
            r[n] -= (n//z - n//(z+1)) *r[z]  

    rinv = [0]*(L+1)            # rinv[d] correspond à R(n//d) pour d<sqrt(N)
    
    for k in range(L,0,-1):      
        n = N//k
        rinv[k] = (n*(n-1))//2
        l = floor(sqrt(n))
        for d in range(2,l+1):  # première somme
            if n//d <= L:
                rinv[k] -= r[n//d]  
            else:
                rinv[k] -= rinv[k*d]
                
        for z in range(1,n//(l+1)+1):  # deuxième somme
            rinv[k] -= (n//z - n//(z+1)) *r[z]  
    return(rinv[1])

t0 = time()
N = 10**8
print( 3*N*(N+1) - 6 - 6*R2(N)  )
print("-- Temps écoulé : ", time()-t0,"s")
