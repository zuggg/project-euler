from time import time

################################################################################
# Méthode 1 : O(N^4), 7.457878112792969 s
################################################################################
# Approche brute :

def triangles(N):
    import itertools as it
    count = 0
    for (x,y) in it.combinations(it.product(range(N+1),repeat=2),2):
        if x==(0,0):
            continue
        a = x[0]**2+x[1]**2
        b = y[0]**2+y[1]**2
        c = (y[0]-x[0])**2+(y[1]-x[1])**2
        if a == b + c or b == a + c or c == a + b:
            print(x,y)
            count +=1                
    return(count)

t0 = time()
N=50
print(triangles(N), time()-t0)

################################################################################
# Méthode 2 : O(N^2), 0.0035538673400878906 s
################################################################################
# Le nombre de triangles rectangles en O est N^2. Le nombre de triangles
# rectangles en P est égal au nombre de triangles rectangles en Q, par symétrie.
# Il suffit donc de compter combien de triangles sont rectangles en P. Pour
# cela, si P=(x,y), il faut et il suffit que Q = P + c*(-y,x), c > 0.
# Étant donné P=(x,y), on 

def gcd(a,b):
    if b==0: return(a)
    else: return(gcd(b,a%b))

def triangle_num(x,y,N):
    d = gcd(x,y)
    return(min( (d*(N-x))//y , (d*y)//x ))

def sum_triangles(N):
    count = 0
    for (x,y) in it.product(range(1,N+1),repeat=2):
        count += triangle_num(x,y,N)
    return(2*count+ 3*N**2)

t0 = time()
N=3000
print(sum_triangles(N), time()-t0)

################################################################################
# Méthode 3 : vraisemblablement encore O(N^2). 
################################################################################
# On utilise la suite de Farey pour générer les (x,y) premiers entre eux.  Soit
# t(N) = #{P(x,y),Q(x',y'), a(P)>a(Q) et OPQ soit rectangle et 0 ≤ x,y,x'x' ≤ N},
# où a désigne l'angle par rapport à l'axe des abscisses (sens trigo).
# * On peut commencer par identifier les triangles rectangles en O : ils
#   correspondent aux couples (i,0),(0,j), donc il y en a N^2.
# * Ensuite, on identifie les triangles rectangles en P et avec x=0 : ce sont
#   les couples (0,i),(j,i), il y en a N^2
# * Idem pour les rectangles en Q avec y'=0.
# * Par symétrie par rapport à la première bissectrice, on peut donc écrire :
#    t(N) = 3*N^2 + #{ OPQ rectangle en P, 1 ≤ x,y,x'x' ≤ N }
#                 + #{ OPQ rectangle en Q, 1 ≤ x,y,x'x' ≤ N }
#         = 3*N^2 + 2*#{ OPQ rectangle en P, 1 ≤ x,y,x'x' ≤ N }
# Maintenant, #{ OPQ rectangle en P, a(P)>a(Q) 1 ≤ x,y,x'x' ≤ N }
#               =  #{ OPQ rectangle en a(P) > a(Q) P, x < y }
#                + #{ OPQ rectangle en a(P) > a(Q) P, x > y }
#                + #{ OPQ rectangle en a(P) > a(Q) P, x = y }
#          =  #{ OPQ rectangle en a(P) > a(Q) P, x < y }
#           + #{ OPQ rectangle en a(P) < a(Q) P, x < y }
#           + #{ OPQ rectangle en a(P) > a(Q) P, x = y }
#          =  #{ OPQ rectangle en P, 1 ≤ x < y ≤ N}
#           + sum k//2 pour k entre 1 et N
#          = #{ OPQ rectangle en P, 1 ≤ x < y ≤ N} + N//2*(N+1)//2
# Finalement, procède comme avant pour dénombrer le nombre de triangles
# rectangles en P(x,y).

def farey_next(a,b,c,d,N):
    k = (N+b)//d
    return(k*c-a, k*d-b)

def count_triangles(N):
    x_old, y_old = 0, 1
    x, y = 1, N
    count = 3*N**2+2*(N//2)*((N+1)//2)
    while y>1:
        for C in range(1,N//y+1):
            count += 2*(min( (N-C*x)//y, (C*y)//x ) + min( (N-C*y)//x, (C*x)//y ) )
        x_old, y_old, x, y = x, y, *farey_next(x_old, y_old, x, y, N)
    return(count)

t0 = time()
N=50
print(sum_triangles(N), time()-t0)
