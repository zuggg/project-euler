
from math import sqrt

def is_perfect_square(n):
    '''Returns True if n is the square of an integer'''
    if type(n)!=int:
        return False
    r = sqrt(n)
    if round(r)**2==n:
        return True
    else:
        return False

# L'aire du triangle (x,x,x+1) est (x+1)/4*sqrt[ (x-1)*(3x+1) ]

f, g = [], []
N = 1000000
for x in range(5,N):
    a = (x+1)*(x+1)*(x-1)*(3*x+1)
    b = (x-1)*(x-1)*(x+1)*(3*x-1)
    if is_perfect_square(a):
        f.append(x)
    if is_perfect_square(b):
        g.append(x)

N = 100000000
x = int(13.9281785663185*2433601)

while(x<N):
    a = (x+1)*(x+1)*(x-1)*(3*x+1)
    if is_perfect_square(a):
        break
    x += 1
f.append(x)

for i in range(6):
    print(f[i+1]/f[i])

# Les quotients semblent converger, ce qui laisse penser que la suite est
# récurrente d'ordre > 1.

from sympy import Matrix
order_f=3
m=Matrix([ f[i:i+order_f+1] for i in range(order_f+1) ] )

A,_=m.rref()

# On trouve la relation de récurrence : x_{n+3} = 15*x_{n+2} - 15*x_{n+1} + x_n
# qui fonctionne pour les deux suites.

# On peut montrer que cette suite engendre toutes les valeurs, mais cela demande
# un peu d'analyse. Quelques idées pour les triangles (x,x,x+1) : rappelons que
# l'aire du triangle est (x+1)/4 * sqrt( (x-1)*(3x+1) ). Si x est pair, alors
# l'entier dans la racine est impair, et donc l'aire est irrationnelle. Il faut
# donc que x=2k-1. L'aire du triangle s'écrit alors
#    k*sqrt( (k-1)*(3k-1) )
# et donc il existe y tq (k-1)*(3k-1) = y^2, ce qui implique que
#    k = (2 + sqrt(1+3*y^2) )/3
# et donc, à nouveau, il existe z tel que 1 + 3*y^2 = z^2. Or, l'équation
#    z^2 - 3*y^2 = 1 
# est une équation de Pell, très liée aux fractions continues. En particulier,
# on en connaît toutes les solutions : la solution fondamentale (celle qui
# minimise z) satisfait (z,y) = (p_k,q_k) où p_k/q_k est un convergent du
# développement en fraction continue de sqrt(3). Ensuite, les autres solutions
# vérifient :
#    z_n + sqrt(3)*y_n = (z_1 +sqrt(3)*y_1)^n.
# Dans notre cas, on trouve vite que
#   |z_n| = | 2  3 |^n |1|
#   |y_n|   | 1  2 |   |0|
# ce qui permet de conclure. On se contente de la très pratique formule de
# récurrence.

from time import time

t0 = time()
N = 10**9                       # périmètre maximal
amax = (N+1)//3                 # côté maximal

f = [1,5,65]
g = [1,17,241]


while True:
    a = 15*f[-1] - 15*f[-2] + f[-3]
    if a <= amax: f.append(a)
    else: break

while True:
    a = 15*g[-1] - 15*g[-2] + g[-3]
    if a <= amax: g.append(a)
    else: break

s = sum( 3*a+1 for a in f ) + sum( 3*a-1 for a in g)

# Somme des prémières (sans compter les triangles plats)
print(s - 6 )
print("Temps écoulé :",time() - t0)
