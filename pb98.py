
def find_anagrams(L):
    n = len(L)
    s = [sorted(w) for w in L]
    sorted_idx = sorted(range(n), key=lambda k: s[k])
    anagrams = []
    for k in range(n-1):
        if s[sorted_idx[k]]==s[sorted_idx[k+1]]:
            anagrams.append((L[sorted_idx[k]],L[sorted_idx[k+1]]))
    return(anagrams)

def map_words(w1,w2):
    '''Returns the permutation that sends w1 onto w2.'''
    n = len(w1)
    s1 = sorted(range(n), key=lambda k: w1[k]) # s1 envoit [ABCDE...] sur w1
    s2 = sorted(range(n), key=lambda k: w2[k]) # s2 envoit [ABCDE...] sur w2
    r = [0]*n 
    for i in range(n):          # r = s2^(-1)
        r[s2[i]] = i
    # s1*r envoit donc w1 sur w2 !
    return( [ s1[r[i]] for i in range(n)] )

def list2int(l):
    '''Converts a list of digits into an int.'''
    s = "".join([ str(d) for d in l])
    return(int(s))

def int2list(n):
    '''Converts an int to a list of its digits.'''
    s = str(n)
    return( [int(d) for d in s] )

def map_number(n,s):
    '''Applies permutation s to number n.'''
    m = int2list(n)
    return( list2int([ m[s[i]] for i in range(len(s)) ] ) )

def word_number(w,n):
    '''Returns true if every letter corresponds to a unique digit in n.'''
    str_n = str(n)
    index = {}
    for i in range(len(w)):
        if str_n[i] in index:
            if index[str_n[i]] != w[i]:
                return False
        else:
            index[str_n[i]] = w[i]
    return True
    

################################################################################

from math import sqrt, ceil
    
with open("p098_words.txt","r") as raw_data:
    words = raw_data.readlines()[0].replace('"','').split(',')
raw_data.close()

n = max(len(w) for w in words)  # longueur maximale des mots
dico = [ [] for _ in range(n) ]

# On commence par créer une liste des mots triés par longueur.
# dico[l-1] contient les mots de longueur l.
for w in words:
    dico[len(w)-1].append(w)

# On collecte les paires d'anagrammes.
anagrams = []
for l in range(1,n):
    anagrams.append( find_anagrams(dico[l-1]) )

# Afin de pouvoir éliminer des nombres de la liste des carrés a priori, on
# compte le nombre de caractères différents dans les anagrammes.
number_of_chars = []
for l in range(n-1):
    number_of_chars.append( set( len(set(v[0])) for v in anagrams[l]) )

stop = False                    
values = set()
for l in reversed(range(2,n)):
    # On génère une liste de carrés avec le bon nombre de caractères différents
    if anagrams[l-1]:
        squares = []
        for i in range( ceil(sqrt(10**(l-1))), int(sqrt(10**l)) ):
            q = i**2
            if len(set(str(q))) in number_of_chars[l-1]:
                squares.append(q)    
    # Il suffit maintenant de trouver les paires de carrés
    for (w1,w2) in anagrams[l-1]:
        s = map_words(w1,w2)
        for d in squares:
            if word_number(w1,d):
                if map_number(d,s) in squares:
                    stop = True
                    print([d, map_number(d,s)],[w1,w2])
                    values.update([d, map_number(d,s)])
    if stop:
        break

# print(max(values))
