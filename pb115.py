

from time import time

t0=time()

m=50
lim = 10**6

f = [1]*(m+1)
n=m

while f[-1] < lim:
    f.append( 1 + sum( f[n-k]*(k-m+1) for k in range(m,n+1) ) ) 
    n+=1

print(n-1,f[-1],time()-t0)


