# timer                          
from datetime import datetime

t0 = datetime.now()

########################################


# Algorithme naïf, mais ça marche en 1,5 s... On pourrait l'améliorer en
# comptant le nombre de bouncy numbers dans chaque centaine, car on sait que n
# est un multiple de 100.

def is_bouncy(n):
    increasing = True
    decreasing = True
    while n>9 and (decreasing or increasing):
        a = n%10
        n = n//10
        b = n%10
        if a>b:
            decreasing = False
        elif a<b:
            increasing = False
    return(decreasing or increasing)

cnt = 1
n = 1
while (n != 100*cnt):
    n += 1
    cnt += is_bouncy(n)

print(n)

# n = 1587000

########################################

print("-- Temps écoulé : ", datetime.now()-t0)
