# timer                          
from datetime import datetime
startTime = datetime.now()

########################################

import math

N=600851475143
i=2

while i<= math.sqrt(N):
    if N%i==0:
        N//=i
    else:
        i+=1

print(N)

########################################

print("-- Temps écoulé :", datetime.now() - startTime)
