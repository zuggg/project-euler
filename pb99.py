
from math import log
from time import time

nums = []
with open("p099_base_exp.txt","r") as raw_data:
    for line in raw_data:
        line=line.strip("\n")
        a=[int(s) for s in line.split(",")]
        nums.append(a)
raw_data.close()


t0 = time()
best = 0
for i in range(len(nums)):
    temp = nums[i][1]*log(nums[i][0])
    if temp > best:
        best = temp
        best_idx = i
print(best_idx+1,"\nTemps écoulé :",time()-t0)
