
# timer                          
from datetime import datetime
startTime = datetime.now()

########################################


# score prend en argument une liste représentant des
# lancés de fléchettes et donne le score correspondant

def score(L):
    if L==[]:
        return(0)
    else:
        F=L.pop()
        if F[0]=="S":
            return(int(F[1:])+score(L))
        elif F[0]=="D":
            return(2*int(F[1:])+score(L))
        elif F[0]=="T":
            return(3*int(F[1:])+score(L))


# Un itérateur qui parcourt tous les checkout de trois fléchettes
# Les arguments sont les une liste de dernière fléchette valide,
# et un liste de toutes les fléchettes

def checkout3(valid, pool):
    n = len(pool)
    if not n:
        return
    indices = [0] * 2
    yield([valid[0]]+[pool[i] for i in indices])
    for f in valid:
        while True:
            for i in reversed(range(2)):
                if indices[i] != n - 1:
                    break
            else:
                indices = [0,-1]
                break
            indices[i:] = [indices[i] + 1] * (2 - i)
            yield([f]+[pool[i] for i in indices])

def checkout2(valid,pool):
    for f in valid:
        for s in pool:
            yield [f]+[s]


# ans renvoit le nombre de manière de checkout avec un score < N

def sol(N,valid,pool):
    cnt = 0
    for L in checkout3(valid,pool):
        if score(L)<N:
            cnt+=1
    for L in checkout2(valid,pool):
        if score(L)<N:
            cnt+=1
    for L in valid:
        if score([L])<N:
            cnt+=1
    return(cnt)

valid = ["D" + str(i) for i in range(1,21)] + ["D25"]
pool  = ["S" + str(i) for i in range(1,21)] + ["S25"] + valid + ["T" + str(i) for i in range(1,21)]
N=100

print(sol(N,valid,pool))

########################################

print("-- Temps écoulé :", datetime.now() - startTime)
