
from time import time

# On peut reprendre l'algorithme du pb76 pour dénombrer les partitions de n, et
# le modifier pour trouver le premier N tq p(N) soit divisible par d.

def d_divides_partitions(d):
    '''Returns the first N such that d divides the number of partitions of N'''
    p=[[1],[1,1]]
    n=2
    while ( sum(p[-1])%d != 0):
        new_line=[1]
        for k in range(1,(n+1)//2):
            new_line.append( (p[n-1][k-1] + p[n-k-1][k])%d )
        for k in range((n+1)//2,n):
            new_line.append( p[n-1][k-1] )
        p.append(new_line+[1])
        n+=1
    return(len(p))

t0 = time()
d_divides_partitions(100000)
print(time()-t0)


# Conclusion : l'algorithme est plutôt rapide pour les petites valeurs de d,
# mais scale très mal. Comme de plus pour N = 100000, la réponse est déjà 11224,
# l'utilisation de mémoire (O(n^2)) explose.
# On essaie donc d'utiliser une formule de récurrence plus rapide et moins
# gourmande en mémoire. Une bonne première approche est le théorème des nombres
# pentagonaux : d'une part,
#    sum p(n)x^n = prod 1/(1-x^k) = prod sum x^{mk} pour k dans N^* et m dans N,
# car p(n) = #{suites (m_k) dans N telles que sum k*m_k =n }, puisque la donnée
# de m_k revient à décider combien de fois k apparaît dans la partition.
# Par ailleurs,
#
#    prod (1-x^k) = 1 - x - x^2 + x^5 + x^7 - x^12 ...
#                 = prod (-1)^k*x^(k*(3*k-1)/2) pour k dans Z
#
# (cf. https://en.wikipedia.org/wiki/Pentagonal_number_theorem)
#
# Par produit de séries formelles, on en déduit que
#    p(n) = sum p(n-i)a_i pour i entre 0 et n, et avec
#
#          | 1  si i = k(3k±1)/2 et k est pair
#    a_i = | -1 si i = k(3k±1)/2 et k est impair
#          | 0  sinon


N=20

def minus_one_pow(n):
    if n%2 == 0:
        return(1)
    else:
        return(-1)

def partition_number(N):
    from math import(sqrt)
    p = [1,1]
    kmax = int(sqrt(2*N/3)+1/6)
    pentagonal_num = []
    for k in range(1,kmax+2):
        pentagonal_num.append( (k*(3*k-1))//2 )
        pentagonal_num.append( (k*(3*k+1))//2 )
    for n in range(2,N):
        idx = 0
        q   = 0
        while (pentagonal_num[idx] <= n):
            q += minus_one_pow(idx//2)*p[-pentagonal_num[idx]]
            idx += 1
        p.append(q)
    return(p)

def d_div_part_num(d):
    p = [1,1]
    pentagonal_num = [1,2,5,7]
    k = 2
    n = 2
    while (p[-1] != 0):
        if (n > pentagonal_num[-3]):
            k+=1
            pentagonal_num.append( (k*(3*k-1))//2 ) 
            pentagonal_num.append( (k*(3*k+1))//2 ) 
        idx = 0
        q   = 0
        while (pentagonal_num[idx] <= n):
            q += minus_one_pow(idx//2)*p[-pentagonal_num[idx]]
            idx += 1
        p.append(q%d)
        n+=1
    return(n-1)

t0=time()
print(d_div_part_num(1000000))
print("Temps écoulé :", time()-t0,"s")
