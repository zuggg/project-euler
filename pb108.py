
# timer                          
from datetime import datetime
startTime = datetime.now()

########################################

# Le cardinal de {(x,y), 1/x+1/y=1/n} est égal à (d_{n^2}+1)/2,
# d_k désigne le nombre de diviseurs de k.
# Par ailleurs, si k=\prod p_i^{a_i}, d_k=\prod (a_i+1),
# donc si n=\prod p_i^{a_i}, d_{n^2}=\prod (2*a_i+1).

# On cherche le plus petit n tq d_{n^2} >= 2*10^3, et donc la suite
# (a_i) est décroissante. On peut par ailleurs majorer sa longueur par 7,
# car 3^7 > 2*10^3.

d= 2*10**3
p=[2,3,5,7,11,13,17]

# Le 8e nombre premier est 19. Comme 5*7 > 19, si 5 et 7 ont un exposant
# au moins égal à 2, on augmente le nombre de diviseurs en enlevant 1 à leur
# exposant et en rajoutant 19, donc pour le n minimal, on sait que l'exposant
# des nb premiers >= 7 est égal à 1.

# Par le même genre d'argument, on sait que l'exposant de 2 est < 8, celui
# de 3 est < 5, et celui de 5 est < 3.

vmax=[7,4,2]

# valeur_n prend en argument un vecteur d'exposants et une liste de nombres
# premiers, et renvoit l'entier correspondant

def valeur_n(v,primes):
    n=1
    for (a,p) in zip(v,primes):
        n*= p**a
    return(n)

# longueur prend en argument un vecteur d'exposants v et un nombre de diviseurs d
# et renvoit la longueur de la suite (a_i), complétée par des 1, de sorte que le
# nombre de diviseurs de n^2 soit >= d.

def longueur(v,d):
    k=len(v)
    div=1
    for a in v:
        div *= 2*a+1
    while div < d:
        div *= 3
        k += 1
    return(k)

# suites_decroissantes renvoit les suites decroissantes d'indices, bornés terme à terme
# par vmax.

def suites_decroissantes(vmax):
    n=len(vmax)
    v=vmax.copy()
    while True:
        yield(v)
        for i in range(1,n+1):
            if v[n-i] > 1:
                v[n-i] -= 1
                for j in range(n-i+1,n):
                    v[j]= min(vmax[j],v[j-1])
                break
        else: 
            return

# meilleur_n renvoit la solution au problème, en prenant en argument une liste
# de nombres premiers, un nombre de diviseurs d, et des bornes supérieures pour
# les exposants

def meilleur_n(primes,d,vmax):
    exposants = [1]*len(primes)
    n= valeur_n(exposants,primes)
    for v in suites_decroissantes(vmax):
        v=v+[1]*(longueur(v,d)-len(v))
        if valeur_n(v,primes) < n :
            exposants = v
            n = valeur_n(v,primes)
    return(n,exposants)
        
print(meilleur_n(p,d,vmax))

# ans: (180180, [2, 2, 1, 1, 1, 1])


########################################

print("-- Temps écoulé :", datetime.now() - startTime)
