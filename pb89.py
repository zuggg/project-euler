# Pas besoin d'explication pour ce problème. On convertit les nombres romains en
# nombres arabes, puis on convertit les nombres arabes en nombres romains
# compacts.

def roman2int(s):
    if len(s)==0:
        return(0)
    elif len(s)==1:
        if s=="M": return(1000)
        elif s=="D": return(500)
        elif s=="C": return(100)
        elif s=="L": return(50)
        elif s=="X": return(10)
        elif s=="V": return(5)
        elif s=="I": return(1)
    else:
        if s[0]=="C" and s[1]=="M": return(900 + roman2int(s[2:]))
        elif s[0]=="C" and s[1]=="D": return(400 + roman2int(s[2:]))
        elif s[0]=="X" and s[1]=="C": return(90 + roman2int(s[2:]))
        elif s[0]=="X" and s[1]=="L": return(40 + roman2int(s[2:]))
        elif s[0]=="I" and s[1]=="X": return(9 + roman2int(s[2:]))
        elif s[0]=="I" and s[1]=="V": return(4 + roman2int(s[2:]))
        else: return(roman2int(s[0]) + roman2int(s[1:]))


def int2roman(i):
    if i < 9:
        if i >= 5: return("V" + int2roman(i-5))
        elif i >= 4: return("IV" + int2roman(i-4))
        else: return("I"*i)
    elif i < 90:
        if i >= 50: return("L" + int2roman(i-50))
        elif i >= 40: return("XL" + int2roman(i-40))
        elif i%10 < 9: return("X"*(i//10) + int2roman(i%10))
        else: return("X"*(i//10) + "IX" + int2roman(i%10 - 9))
    elif i < 900:
        if i >= 500: return("D" + int2roman(i-500))
        elif i >= 400: return("CD" + int2roman(i-400))
        elif i%100 < 90: return("C"*(i//100) + int2roman(i%100))
        else: return("C"*(i//100) + "XC" + int2roman(i%100 - 90))
    else:
        if i%1000 < 900: return("M"*(i//1000) + int2roman(i%1000))
        elif i%1000 >= 900: return("M"*(i//1000) + "CM" + int2roman(i%1000 - 900))

    
numerals=[]
with open("p089_roman.txt","r") as raw_data:
    for line in raw_data:
        line=line.strip("\n")
        numerals.append(line)
raw_data.close()
char_num = sum(len(r) for r in numerals)

integers=[roman2int(r) for r in numerals]

numeral_better=[int2roman(i) for i in integers]
char_num_opt = sum(len(r) for r in numeral_better)

print(char_num - char_num_opt)
