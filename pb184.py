import math
# timer                          
from datetime import datetime

t0 = datetime.now()

########################################

n=100


def gcd(x,y):
    while y:
        x, y = y, x%y
    return x

# total_points renvoit le nombre total de points à coordonnées entières dans le
# cercle de rayon n
# def total_points(n):
#     N=sum( math.ceil(math.sqrt(n**2-x**2)) for x in range(1,n) )
#     return(4*N+1)

# number_of_points renvoit le nombre de points à coord. entières sur
# le rayon passant par (p,q), si gcd(p,q) = 1
def number_of_points(p,q,n):
    return(math.ceil(n/math.sqrt(p**2+q**2))-1)

# coef_lines renvoit une liste avec autant une entrée pour chaque rayon du
# premier cadran qui contient des points à coord. entières, et l'entrée
# correspondante est le nombre de points à coord. entière pour ce rayon
def coef_lines(n):
    L=[number_of_points(0,1,n)]
    for x in range(1,n):
        for y in range(1,math.ceil(math.sqrt(n**2-x**2))):
            if gcd(x,y)==1:
                L.append(number_of_points(x,y,n))
    return(L)
                

# Le nombre de triangles est donné par
# 3S = 4*\sum_{i1<i2} Q(p1,p2) + \sum Q(p,p)
# avec Q(p1,p2) = p1*p2*(N - 1 - 2*p1 - 2*p2)
def number_of_triangles(n):
    S = 0
    coef = coef_lines(n)

    # for (p1,p2) in it.combinations(coef,2):
    #     S += 4*p1*p2*(N-2*p1-2*p2-1)
    # for p in coef:
    #     S += p * p * (N - 1 - 4*p)

    S1 = sum( coef )
    S2 = sum( p**2 for p in coef )
    S3 = sum( p**3 for p in coef )
    
    S= 4*(2*S1**3 - 3*S1*S2 + S3)

    return(S//3)

print(number_of_triangles(n))


########################################

print("-- Temps écoulé : ", datetime.now()-t0)
