
import math

# timer                          
from datetime import datetime

t0 = datetime.now()

########################################

# L'enveloppe des paraboles décrites par les particules est encore un
# paraboloïde de révolution, dont la section est d'équation
# y = -A*x^2 + 1/(4*A) + 100
# Par conséquent, le volume correspondant est donné par
# int_{y=0}^{ymax} \pi x^2 dy = \pi/(2*A) * ymax^2
#                             = \pi/(2*A) * (1/(4*A)+100)^2

A=9.81/800

print(math.pi/(2*A)*(1/(4*A)+100)**2)

# answer = 1856532.8455


########################################

print("-- Temps écoulé : ", datetime.now()-t0)
