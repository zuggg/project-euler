# Rien d'extraordinaire. Il suffit de faire une exponentiation rapide dans Z/NZ.
# Une approche bruteforce fonctionne aussi.

from time import time

def pow_mod(a,b,N):
    if b == 1:
        return( a )
    elif b%2 == 0:
        return( pow_mod( (a*a)%N, b//2, N) )
    else:
        return( a*pow_mod( a, b-1, N)%N )
    
t0=time()
N=10**10
print((28433*pow_mod(2,7830457,N)+1) %N, "\nTemps écoulé :", time()-t0)
