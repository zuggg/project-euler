import numpy as np
import itertools as it

# timer                          
from datetime import datetime
startTime = datetime.now()


########################################

# autre version de test_sss1 et 2 présents dans pb105, mais avec itertools


def test_sss1(v):
    n=len(v)
    return( sum( v[:(n+1)//2 ] ) - sum( v[n//2+1:] ) > 0 )

def test_sss2(v):
    n=len(v)
    for k in range(2,n//2+1):
        for p in it.combinations(v,2*k):
            for sp in it.combinations(p[1:],k-1):
                s1 = sum(sp) + p[0]
                s2 = sum(p)
                if 2*s1==s2:
                    # print([v[c[j]] for j in p])
                    # print([v[i] for i in c])
                    return(False)
    return(True)

def bin(k,n):
    if k==0:
        return 1
    else:
        return n*bin(k-1,n-1)//k

def opt_sss(N,amax):
    amin=max(1,N-1)
    w=[amax]*N
    cnt=0
    percent_nb_test = bin(N, amax-amin)//100
    print(percent_nb_test)
    for v in it.combinations(range(amin,amax),N):
        cnt+=1
        if test_sss1(v) and test_sss2(v):
            if sum(v)<sum(w):
                w = v
        if cnt%percent_nb_test == 0:
            print(cnt//percent_nb_test)
    print(w)
    
opt_sss(7,50)


########################################

print("-- Temps écoulé :", datetime.now()-startTime)
