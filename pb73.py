########################################

from time import time
from math import sqrt, ceil
import numpy as np

########################################

def gcd(a,b):
    while b:
        a, b = b, a%b
    return(a)

# liste des nombres premiers < N
def generate_primes(N):
    # crible d'Ératosthène
    primes=np.ones(N, bool)
    primes[0],primes[1]=False, False
    
    for i in range(2,N//2):
        primes[i*2]=False

    for p in range(3,ceil(sqrt(N))):
        if primes[p]:
            i=p
            while i*p < N:
                primes[i*p]=False
                i+=2
        else:
            continue
            
    primes_list=[]
    for p,b in enumerate(primes):
        if b:
            primes_list.append(p)
    return(primes_list)



################################################################################
# Méthode 1 : complexité : O(N^2*log(N)), temps d'éxécution : 9.7 s
################################################################################
# Approche la plus naïve : on dénombre bêtement.
# Pour un q fixé, p/q > 1/3 => p > q/3 et p/q < 1/2 => p < q/2 donc le nombre
# total de fractions (non-réduites) entre 1/3 et 1/2 qui ont pour dénominateur q
# est environ q/2 - q/3 = q/6
#    sum q/6 pour q entre 4 et 12000 < 1/12 * 12000*12001 = 12 001 000

t0 = time()
count = 0

N=12000
for q in range(5,N+1):
    for p in range(q//3+1,(q-1)//2+1):
        if gcd(p,q)==1:
#            print(p,q)
            count += 1

print(count)

# count = 7295372

print("-- Temps écoulé : ", time()-t0,"s")

################################################################################
# Méthode 2 : complexité : O(N^2), temps d'éxécution : 2.4 s
################################################################################
# Avec les suites de Farey (cf l'article sur l'arbre de Stern-Brocot pour plus
# de détails) : l'idée est qu'à partir de deux termes consécutifs d'une suite de
# Farey, a/b < c/d, alors la fraction (réduite) de plus petit dénominateur
# comprise entre a/b et c/d est leur médiane, càd (a+c)(b+d).  Or, 1/3 et 1/2
# sont deux termes consécutifs de F_3, on peut donc générer toutes les fractions
# entre 1/3 et 1/2 dont le dénominateur est < N en parcourant l'arbre
# correspondant. Par ailleurs, il suffit de regarder les dénominateurs.


def SB(N):
    left = 3
    right = 2
    count = 0
    stack=[]
    while True:
        med = left + right
        if med > N:
            if stack:
                left = right
                right = stack.pop()
            else:
                break    
        else:
            count+=1
            stack.append(right)
            right = med
    return(count)
            
t0 = time()
N=12000
print(SB(N))
print("-- Temps écoulé : ", time()-t0,"s")

################################################################################
# Méthode 3 : complexité O(N), temps d'exécution : 2.5 ms
################################################################################
# En utilisant la formule d'inversion de Moebius : soit F(N) le nombre de
# couples (p,q) tq 1/3 < p/q < 1/2, et 0< q <=N, et soit R(N) le nombre de
# couples qui vérifient les mêmes conditions, avec en plus pgcd(p,q)=1.  Or, on
# se rend compte assez facilement que
#    F(N) = sum R(N//m) pour m entre 1 et N,
# et donc la formule d'inversion de Moebius donne
#    R(N) = sum mu(m)F(N//m) pour m entre 1 et N.
# Dans notre cas, F(N) se calcule explicitement, on trouve :

def F(n):
    k = n//6
    r = n%6
    return( k*(3*k-2+r) + (r==5)  )

# Il suffit donc de déterminer les valeurs de la fonction de Moebius. Pour cela,
# on a besoin de la liste des nombres premiers < N, ou même mieux si on remarque
# que k < 5 => F(k) = 0, on a besoin des nombres premiers <= N/5.  Par ailleurs,
# on peut écrire une fonction récursive plutôt que de calculer les valeurs de la
# fonction de Moebius, grâce à la formule :
#   R(N) =  F(N) - sum F(N//p)  pour p premier entre 1 et N
#                + sum F(N//pq) pour q premier, p < q < N
#                - sum F(N//pqr) ...


def R(N,index=0):
    count = F(N)
    while (index < len(primes) and primes[index]*5 <= N):
        count -= R(N//primes[index], index+1)
        index += 1
    return(count)

t0 = time()

N = 12000
primes = generate_primes(N//5+2)
print(R(N))

print("-- Temps écoulé : ", time()-t0,"s")



