
import numpy as np
from math import sqrt, ceil

########################################

# timer                          
from datetime import datetime

########################################

t0 = datetime.now()

# liste des nombres premiers < N
def generate_primes(N):
    # crible d'Ératosthène
    primes=np.ones(N, bool)
    primes[0],primes[1]=False, False
    
    for i in range(2,N//2):
        primes[i*2]=False

    for p in range(3,ceil(sqrt(N))):
        if primes[p]:
            i=p
            while i*p < N:
                primes[i*p]=False
                i+=2
        else:
            continue
            
    primes_list=[]
    for p,b in enumerate(primes):
        if b:
            primes_list.append(p)
    return(primes_list)


def prime_decomp(n):
    lim = int(sqrt(n))
    for p in plist:
        if p > lim:
            break
        if n%p == 0:
            n //= p
            mult = 1
            while n%p==0:
                mult += 1
                n //= p
            yield([p,mult])
            lim = int(sqrt(n))
    if n > 1:
        yield([n,1])
    return


def factor(n):
    divisors = []
    for p in prime_decomp(n):
        divisors.append(p)
    return(divisors)
        

def phi(n):
    val = n
    for [p,m] in prime_decomp(n):
        val = val//p * (p-1)
    return val

########################################

N = 10**7            
plist = generate_primes(int(math.sqrt(N)))

#for [p,m] in prime_decomp(87109):
#    print(p)


# phi(87109) = 79180, donc on a déjà un candidat. Comme 87109 = 11 * 7919, on en
# déduit que le n qui minimise n/phi(n) n'est pas divisible par 2, 3, 5, ou 7.
# Seuls 48 entiers entre 1 et 210 vérifient cette propriété, et il suffit de
# tester les nombres qui leur sont congrus modulo 210.

# On cherche à éliminer un maximum de nombres premiers en trouvant un bon
# candidat < 10^5.

r = 2
for i in range(11,10**5):
    if i%2==0 or i%3==0 or i%5==0 or i%7==0:
        continue
    j = phi(i)
    if sorted(str(i)) == sorted(str(j)):
        if i/j < r:
            candidat = i
            r = i/j

# On veut que tous les facteurs premiers p vérifient p/(p-1) < r, donc p >
# r/(r-1) ; ici, on en déduit que p > 115.

forbidden_primes = [p for p in plist if p < ceil(r/(r-1))]

r = 2
for i in range(2,10**7):
    for p in forbidden_primes:
        if i%p == 0:
            break
    else:
        j = phi(i)
        if sorted(str(i)) == sorted(str(j)):
            if i/j < r:
                candidat = i
                r = i/j


print(candidat)

# candidat = 8319823

# Cet algorithme est très lent (~38s), mais il a l'avantage de ne pas prendre
# trop de mémoire. L'analyse permet de grandement améliorer l'efficacité de
# cette approche, par exemple en trouvant un candidat proche de 10^7 qui est
# produit de deux nombres premiers assez grands, pour en déduire que le n
# optimal ne peut pas être facteur de 3 nombres premiers, puis explorer les
# produits de deux nombres premiers proches de sqrt(10^7).

# Comme 10^7 n'est pas trop grand, on aurait pu aussi simplement calculer toutes
# les valeurs de phi grâce à un crible, qui pèserait 40 Mo avec des entiers de
# 32 bits.


########################################
    
print("-- Temps écoulé : ", datetime.now()-t0)
