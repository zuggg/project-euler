#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  2 16:12:04 2020

@author: thibaud
"""

from tkinter import *
from math import sqrt, cos, sin, pi

################################################################################

class points():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.angle = pi/2
        self.age = 1
    
    def motionleft(self):
        self.x += echelle * cos( self.angle + pi/5 )
        self.y += echelle * sin( self.angle + pi/5 )
        self.angle += 2*pi/5
        self.age += 1
        
    def motionright(self):
        self.x += echelle * cos( self.angle - pi/5 )
        self.y += echelle * sin( self.angle - pi/5 )
        self.angle -= 2*pi/5
        self.age += 1

def erase():
    can1.delete(ALL)
    initiate()
    
def initiate():
    A.x = 0
    A.y = 0
    A.angle = pi/2
    draw_point(A.x,A.y,"red")
    
def newleft():
    x0, y0 = A.x, A.y
    draw_point(A.x,A.y,"green")
    draw_arc_left(A)
    A.motionleft()
#    can1.create_line(width//2 + x0, height//2 - y0, width//2 + A.x, height//2 - A.y)
    draw_point(A.x,A.y,"red")
    
def newright():
    x0, y0 = A.x, A.y
    draw_point(A.x,A.y,"green")
    draw_arc_right(A)
    A.motionright()
#    can1.create_line(width//2 + x0, height//2 - y0, width//2 + A.x, height//2 - A.y)
    draw_point(A.x,A.y,"red")


def draw_point(x,y, col = "red"):
    if col == "red":
        color = "#FF0000"
    elif col == "green":
        color = "#00FF00"
    r = 2
    x1, y1 = width//2 + (x - r), height//2 - (y - r)
    x2, y2 = width//2 + (x + r), height//2 - (y + r)
    can1.create_oval(x1, y1, x2, y2, fill=color)

def draw_arc_right(A):
    xc =   A.x + r*sin( A.angle ) +  width//2 
    yc = - A.y + r*cos( A.angle ) +  height//2
    t0 = 18 + A.angle*180/pi
    can1.create_arc(xc-r, yc-r, xc+r, yc+r, start=t0, extent=72, style='arc', width=1)

def draw_arc_left(A):
    xc =   A.x - r*sin( A.angle ) +  width//2 
    yc = - A.y - r*cos( A.angle ) +  height//2
    t0 = - 90 + A.angle*180/pi
    can1.create_arc(xc-r, yc-r, xc+r, yc+r, start=t0, extent=72, style='arc', width=1)

################################################################################

width = 600
height = 600
echelle = 100
r = sqrt( echelle**2/(2-2*cos(2*pi/5)) )

A = points()

fen1 = Tk()
bou1 = Button(fen1, text='Quitter', command = fen1.destroy)
bou2 = Button(fen1, text='Left', command = newleft, bg='#FFAAAA')
bou3 = Button(fen1, text='Right', command = newright, bg='#FFAAAA')
bou4 = Button(fen1, text='Initiation', command = initiate)
bou5 = Button(fen1, text='Erase', command = erase)

can1 = Canvas(fen1, bg='dark grey', height=height, width=width)

bou1.grid(row = 0, column = 0)
bou2.grid(row = 0, column = 1, sticky = E)
bou3.grid(row = 0, column = 2, sticky = W)
bou4.grid(row = 0, column = 3)
bou5.grid(row = 0, column = 4)

can1.grid(row = 1, column = 0, columnspan = 5)

draw_point(A.x,A.y,"red")

fen1.mainloop()


