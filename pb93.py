
import itertools as it

################################################################################
# Méthode 1 : brute force sale
################################################################################
# On forme et évalue des expressions arithmétiques. C'est très inefficace.

operators = "+-*/"
possible_values = set()

for digits in it.permutations([2,3,4,5]):
    for op in it.product(operators,repeat=3):
        for p in paren:
            possible_values.add(arithmetic_expression(digits,op,p))
            
len(sorted(d for d in possible_values if d >0))


def arithmetic_expression(digits,op,par):
    op = [o for o in op]
    digits = [d for d in digits]
    expr_string = ""
    opening_digit = True
    opening_par = True
    for c in par:
        if c=="x":
            opening_par = False
            if opening_digit:
                expr_string += str( digits.pop() )
                opening_digit = False
            else:
                expr_string += op.pop() + str( digits.pop() )
        elif c=="(":
            opening_digit = True
            if opening_par:
                expr_string += "("
            else:
                expr_string += op.pop() + "("
                opening_par = True
        else:
            expr_string += ")"
    try:
        v = eval(expr_string)
    except ZeroDivisionError:
        return(0)

    if (type(v)==int):
        return(v)
    else: return(0)

for p in paren:
    arithmetic_expression([1,2,3,4],["+","+","-"],p)

paren = [
    "xxxx",
    "(xx)xx",
    "x(xx)x",
    "xx(xx)",
    "(xx)(xx)",
    "(xxx)x",
    "x(xxx)",
    "((xx)x)x",
    "(x(xx))x",
    "x((xx)x)",
    "x(x(xx))"
]

################################################################################
# Méthode 2 : brute force un peu plus propre
################################################################################
# Toutes les expressions arithmétiques peuvent s'écrire de la forme
#    o1( a, o2( b, o3( c, d)))
# où o1, o2 et o3 sont des opérateurs binaires parmi +, -, * et /, ainsi que les
# duals des opérateurs antisymétriques - et /, d- et d/
# (par exemple, d-(a, b) = -(b, a) = b - a).

def operations(seta,setb):
    '''Computes all the possible operations (+,-,*,/) between seta and setb.'''
    values = set()
    for (a,b) in it.product(seta,setb):
        if b==0 and a!=0:
            values = values.union([a, -a, 0])
        elif a==0:
            values = values.union([b, -b, 0])
        else:
            values = values.union([a+b, a-b, b-a, a*b, a/b, b/a])
    return(values)

def all_expr(digits):
    '''Generates possible evaluations of arithmetic expressions for given ordered digits'''
    if len(digits)==1:
        return(set(digits))
    else:
        val1 = all_expr(digits[:1])
        val2 = all_expr(digits[1:])
        values = set().union( operations( val1, val2 ) )
        return(values)


def print_targets(digits):
    digits = [Fraction(d) for d in digits]
    possible_values = set()
    for p in it.permutations(digits):
        new_values = [int(p) for p in all_expr(p) if p > 0 and p.denominator==1]
        possible_values = possible_values.union(new_values)
    possible_values = sorted(possible_values)  # conversion en liste ordonnée
    return(possible_values)
    
def consecutive_targets(digits):
    '''Returns the number of consecutive integers that can be formed using digits'''
    possible_values = set()
    for p in it.permutations(digits):
        new_values = [int(p) for p in all_expr(p) if p > 0 and p.denominator==1]
        possible_values = possible_values.union(new_values)
    possible_values = sorted(possible_values)  # conversion en liste ordonnée
    for i in range(len(possible_values)):
        if possible_values[i] != i+1:
            break
    else:
        i = len(possible_values)
    return(i)


from time import time
from fractions import Fraction
t0 = time()

best = 0
for digits in it.combinations(range(1,10),4):
    x = consecutive_targets([Fraction(d) for d in digits])
    if x == best:
        print(digits)
    if x > best:
        best = x
        best_digits = digits
        print(digits, x)

print(time()-t0)
