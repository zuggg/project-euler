
import numpy as np

# timer                          
from datetime import datetime
startTime = datetime.now()


########################################


gen=lambda n:(1+n**11)/(1+n)
deg=10


x=[*range(1,deg+2)]
y=[*map(gen, x)]

# print(x,y)

s=0


def monome(k,i,X):
    global x
    return(np.prod( [(X-x[j])/(x[i]-x[j]) for j in range(k) if j != i] ) )
    

for k in range(1,deg+1):
    term=sum( y[i]*monome(k,i,k+1) for i in range(k))
    # print(term)
    if term!=y[k]:
        s+=term

print(s)

########################################

print("-- Temps écoulé :", datetime.now()-startTime)
