# timer                          
from datetime import datetime
startTime = datetime.now()

########################################

# on cherche la différence entre la (somme des entiers) au carré et la somme des (entiers au carré).
# Or, la somme des entiers au _cube_ est égale à la (somme des entiers) au carré, donc cette différence
# s'écrit facilement.

N=100

S=0

for i in range(1,N+1):
   S+=i**2*(i-1)

print(S)



########################################

print("-- Temps écoulé :", datetime.now() - startTime)
