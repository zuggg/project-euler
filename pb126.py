
from time import time

# Pour un pavé p×q×n, la (k+1)-ième couche contient
#    L_(k+1) = 2*(n*p + n*q + p*q) + 4*k*(n + p+q + k - 1)
# cubes.

# On génère toutes les couches de taille ≤ N.
def layers(N):
    count = [0]*(N+1)
    nmax = (N-1)//2 + 1
    for n in range(1, nmax+1):
        pmax = min(n, (N-n)//(n+1) )
        for p in range(1,pmax+1):
            qmax = min(p, (N-n*p)//(n+p) )
            for q in range(1,qmax+1):
                L = p*n+n*q+q*p
                s = p + q + n
                k = 0
                while L <= N:
                    count[L]+=1
                    L += 2*(s+2*k)
                    k += 1
    return count



t0 = time()
c = layers(10000)
print(2*c.index(1000), time()-t0)


