
from time import time

# Sur une partie à n manches, la probabilié de tirer exactement n - k disques
# bleus est
#    P(n-k) = 1/(n+1)! sum i1 * i2 * ... * ik, pour 1 ≤ i1 < i2 < ... < ik ≤ n.
# On reconnaît donc le k-ième polynôme symétrique élémentaire en n variables,
# évalué en (1,2,3,...,n).
# Par ailleurs, en les notant s_k^n (T1,...,Tn), ces polynômes satisfont à la
# relation de récurrence suivante :
#   s_0^n = 1
#   s_k^n = s_k^(n-1) + Tn * S_(k-1)^(n-1)  pour 0 < k < n

def s(T):
    '''Returns the list of elementary symmetric ponymials in T'''
    n = len(T)
    U = [1]
    for k in range(n):
        V = [1]
        for j in range(k):
            V.append( T[k]*U[j] + U[j+1] )
        V.append( T[k]*U[k] )
        U = V.copy()
    return(U)

def fact(n):
    if n == 0 or n == 1:
        return 1
    else:
        return(n*fact(n-1))

# Pour ce problème, on peut optimiser un peu car on cherche seulement la
# première moitié des valeurs de s(T).
# Si la probabilité de gagner est P, le joueur perd de l'argent tant que la
# cagnotte est inférieure à 1/P. 

t0 = time()

N = 300
p = s([d for d in range(1,N+1)])

print( fact(N+1)//sum( p[:(N+1)//2] ) , time()-t0)

# Algorithme plutôt efficace, complexité en 0(N^2) pour le calcul des polynômes
# symétriques élémentaires. 
