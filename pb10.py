# timer                          
from datetime import datetime
import numpy as np
import math
startTime = datetime.now()

########################################

N=2000000

# crible d'Ératosthène
primes=np.ones(N, bool)
primes[0],primes[1]=False, False

s=0

for p in range(2,math.ceil(math.sqrt(N))):
    if primes[p]:
        s+=p
        i=p
        while i*p<N:
            primes[i*p]=False
            i+=1
    else:
        continue


########################################

print("-- Temps écoulé :", datetime.now()-startTime)
