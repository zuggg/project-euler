# timer                          
from datetime import datetime

########################################

t0 = datetime.now()

# Pour un q donné, on cherche p tel que p/q < 3/7 < p+1/q

r=1
for q in range(1,1000000):
    p = 3*q//7
    if 7*p==3*q:
        continue
    if 3/7-p/q < r:
        pe,qe = p,q
        r = 3/7-p/q

print(pe,qe)

########################################
    
print("-- Temps écoulé : ", datetime.now()-t0)
