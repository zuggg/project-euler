
from time import time
from mpmath import sqrt, mp

# On pourrait coder un algorithme qui donne la racine carrée d'un nombre à
# précision arbitraire grâce à l'algorithme de Newton, mais python propose déjà
# cette solution avec mpmath.

def sod(x,n):
    '''Returns the sum of the n first decimal digits of x'''
    decimal_digits = str(x).replace(".","")
    if len(decimal_digits) < n:
        return(0)
    else:
        return( sum( int(d) for d in decimal_digits[:n] ) ) 
    


t0 = time()
n = 100000
mp.dps = n+10                    # on prend un peu de marge
S = 0
for i in range(2,100):
    S += sod(sqrt(i),n)
print(S)
print("Temps écoulé :",time()-t0,"s")
