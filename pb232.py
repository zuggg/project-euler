
from random import randint, random

# Une simulation naïve d'une partie :

def game(N):
    countA, countB = 0, 0
    while True:
        countA += randint(0,1)
        countB += randint(0,1)
        if countA >= N:
            return("A")
        if countB >= N:
            return("B")


################################################################################

        
# Soit p(i,j) la probabilité que B gagne, sachant que A et B ont respectivement
# i et j points, et c'est à B de jouer. La probabilité que B gagne depuis le
# début est alors 1/2*( p(1,0) + p(0,0) ).
# Si B ne pouvait lancer la pièce qu'une seule fois pas tour (comme A), alors on
# aurait la formule de récurrence :
#    p(i,j) = 1/4 * ( p(i,j) + p(i+1,j) + p(i,j+1) + p(i+1,j+1) ).
# Cependant, B peut choisir de lancer la pièce T fois, et gagner 2^(T-1) points,
# et donc la formule devient
#    p(i,j) = max_T { (2^T - 1)/(2^T + 1)*p(i+1,j)
#                         + 1/(2^T + 1)*[ p(i+1,j+2^(T-1)) + p(i,j+2^(T-1)) ] }
# On voit qu'il ne sert à rien de prendre un T très grand : à partir du moment
# où j + 2^(T-1) > n, les deux probabilités du second membre valent 1, mais le
# facteur devant p(i+1,j) devient plus grand, donc ce terme < 1 devient plus
# important dans la combinaison convexe. Ainsi, en pratique, le max est atteint
# pour T - 1 ≤ Tmax - 1 = log2(n) + 1.


def prob_B_wins(n):
    from math import log2
    
    p = [ [0]*n for _ in range(n+1) ]

    # Chaque case de p dépend donc des valeurs des cases en bas et à droite. On
    # peut donc mettre à jour p ligne par ligne, en partant d'en bas à droite,
    # par exemple.

    Tmax = int(log2(n)) + 2
    
    a = [ (2**T-1)/(2**T+1) for T in range(1,Tmax+1) ]
    b = [   1/(2**T+1) for T in range(1,Tmax+1) ]

    for i in reversed(range(n)):
        for j in reversed(range(n)):
            possible_values = []
            for T in range(Tmax):
                if j+2**T < n:
                    possible_values.append( a[T]*p[i+1][j] + b[T]*( p[i+1][j+2**T] + p[i][j+2**T]) )
                else:
                    possible_values.append( a[T]*p[i+1][j] + b[T]*2 )
                    break

            p[i][j] = max(possible_values)

    print( '{:.8f}'.format((p[0][0] + p[1][0])/2) )
    return

from time import time

t0 = time()
prob_B_wins(100)
print("Temps d'exécution :", time()-t0)
