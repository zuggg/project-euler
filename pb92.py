
from time import time

def square_of_digits(n):
    return(sum(int(d)**2 for d in str(n)))

################################################################################
# Méthode 1 : bruteforce très coûteux.
################################################################################

def f(N):
    # Initialisation d'un tableau des petits valeurs
    lim = 81 * (len(str(N))-1)
    arrives_at_89 = {}
    to_test = set(range(2,lim+2))
    to_test.discard(89)
    
    arrives_at_89[89] = 1
    arrives_at_89[1] = 0

    j = to_test.pop()
    stack = [j]
    
    while to_test:
        j = square_of_digits(j)
        if j in arrives_at_89:
            for k in stack:
                arrives_at_89[k] = arrives_at_89[j]
            to_test.difference_update(stack)
            j = to_test.pop()
            stack = [j]
        else:
            stack.append(j)

    
    count = sum(arrives_at_89.values())

    for i in range(lim+1,N):
        if arrives_at_89[square_of_digits(i)]:
            count += 1
    return(count)


t0 = time()
N = 10**7
print(f(N), time()-t0)

# 8581146
# C'est très lent (>30s), parce qu'il y a beaucoup d'itérations.

################################################################################
# Méthode 2 : complexité en O(log(N)), temps d'exécution 1 s pour N = 10**100
################################################################################
# Autre méthode plus maligne, lorsque N est une puissance de 10 : on peut
# procéder par récurrence. On commence par établir un tableau des petites
# valeurs, en l'occurence < 81 * (nombres max de chiffres), et on crée en même
# temps un tableau de comptage. Dans le tableau de comptage, la case k
# correspond au nombre de nombres n tq la somme de leur chiffres au carré est
# égale à k. Par exemple, à la première itération, càd pour les nombres n<10,
# count[1] = 1, count[4] = 1, ... count[81] = 1.
# À l'étape suivante, on comptabilise les nombres <100. Il suffit alors de
# translater le tableau 9 fois :
#    count_update[i] = count[i] + count[i-1] + count[i-4] + ... + count[i-81]
# et ainsi de suite, jusqu'à arriver à la dernière étape. Une fois la récursion
# terminée, il suffit de multiplier par le booléen (i arrive à 89).
# Complexité en O(log10(N)).

def g(k):                       # k désigne le nombre de chiffres maximum
    # on commence par créer un tabeau des petites valeurs.
    max_val = max(2, k)*81 + 1

    values = [0]*max_val
    values[89] = 89
    values[1] = 1

    for i in range(2,max_val):
        if values[i]==0:
            stack = [i]
            sod = square_of_digits(i)
            while values[sod]==0:
                stack.append(sod)
                sod = square_of_digits(sod)
            for v in stack:
                values[v] = values[sod]

    squares = [d**2 for d in range(10)]
    count = [0] * max_val
    for d in squares: count[d] = 1  # première étape à la main pour gagner du temps
    for j in range(2,k+1):
        for v in range(j*81, 0, -1):  
            count[v] = sum( count[v-d] for d in squares if v-d >= 0)

    total = 0
    for i in range(max_val):
        if values[i] == 89:
            total += count[i]

    return(total)

t0 = time()
k = 100
print(g(k), time()-t0)
