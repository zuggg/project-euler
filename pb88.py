# Soit m(k) le plus petit nombre produit-somme de cardinal k. On trouve, à la
# main :
#
#  k   |  m(k) |  N correspondant
# –––––+–––––––+–––––––––––––––––
#  2   | 4     | 2,2
#  3   | 6     | 1,2,3
#  4   | 8     | 1,1,2,4
#  5   | 8     | 1,1,2,2,2
#  6   | 12    | 1,1,1,1,2,6
#  7   | 12    | 1,1,1,1,1,3,4
#  8   | 12    | 1,1,1,1,1,2,2,3
#  9   | 15    | ...,1,3,5
#  10  | 16    | ...,1,4,4
#  11  | 16    | ...,1,2,2,4
#  12  | 16    | ...,1,2,2,2,2
#  13  | 18    | ...,1,2,3,3
#
# De manière générale, m(k) > k et
# ...,1,2,k => m(k) ≤ 2*k.
# ...,1,3,b => m(2*b-1) ≤ 3*b
# ...,1,4,b => m(3*b-2) ≤ 4*b
# de manière générale, ...,1,a+1,b+1 => m(a*b+1) ≤ (a+1)*(b+1)

# Par exemple : 12000 = 13*13*71 + 1, donc m(12000) ≤ 170*72 = 12240

from math import log, ceil
from time import time

def produit(v):
    p = 1
    for i in v:
        p *= i
    return(p)

def m(k):
    r = ceil(log(k,2))
    seq = [3,3]+[1]*(r-2)
    sums = [2*k]
    best = 2*k
    while True:
        a = produit(seq[1:])
        while a*seq[0] < best:
            prod = produit(seq)
            if prod==(sum(seq)+k-r):
                sums.append(prod)
                best = prod
            seq[0] += 1
        rang = 1
        while rang < r and produit(seq) >= best:
            seq[rang] += 1
            for i in range(rang):
                seq[i] = seq[rang]
            rang += 1
        if rang >= r and produit(seq) >= best:
            return(min(sums))


################################################################################
# Méthode 1 
################################################################################
# On calcule tous les m(k), pour k entre 2 et 12000. C'est très coûteux, car on
# ne tient pas compte des calculs précédents. Temps d'exécution : 1700 s

t0=time()      
m_vals=[]
for k in range(2,12001):
    m_vals.append(min(m(k)))
    if k%1000==0: print(k)
print("Travail terminé.",time()-t0)

result = []
for v in m_vals:
    if v not in result:
        result.append(v)
ans = sum(result)

# ans = 7587457

################################################################################
# Méthode 2
################################################################################
# À la place de calculer tous les m(k), on utilise les bornes a priori sur m(k)
# pour générer tous les produits potentiels, et les séquences produit-somme
# correspondantes. En l'occurence, on peut utiliser le fait que pour tout k ≤ N,
# m(k) ≤ 2*N. Je ne sais pas s'il est possible d'obtenir une meilleure borne a
# priori car
# i/ m(k) n'est pas croissante
# ii/ pour k = 24, 114, 174, 444, m(k)=2*k. Ce sont les seuls k ≤ 12000 qui
#     vérifient cette propriété, mais peut-être qu'il existe une infinité de
#     tels k.
# (fonctions foo par luftbahnfahrer)

def foo(factors,fprod,fsum,flen,facmin):
    global m_values
    k = fprod - fsum + flen
    if k<=N : m_values[k] = min(m_values[k], fprod)
    newfac = facmin
    while fprod*newfac <= 2*N :
        foo(factors+[newfac], fprod*newfac, fsum+newfac, flen+1, newfac)
        newfac += 1
    return

t0 = time()
N = 12000
m_values = [2*k for k in range(N+1)]
foo([2,2],1,0,0,2)
print(sum(set(m_values[2:])),"\nTemps écoulé :",time()-t0)



################################################################################
# Méthode 3 
################################################################################
# (par bretthclarke sur le forum). Similaire à la méthode 2 dans l'esprit. Pour
# chaque entier i en partant de 2, et jusqu'à ce qu'on ait trouvé tous les N(k)
# pour k ≤ 12000, on factorise i. À chaque factorisation possible correspond une
# séquence [a_1, ... a_k] qui vérifie sum a_j = prod a_j -> on enregistre la
# longueur de cette séquence dans un dictionnaire mem.
# L'astuce est d'utiliser une récurrence pour générer les factorisations : par
# exemple, en sachant
# 2 : {1}        <- 2 = 2
# 3 : {1}        <- 3 = 3
# 4 : {1,2}      <- 4 = 4 ; 2 + 2 = 2 * 2
# 6 : {1,3}      <- 6 = 6 ; 1 * 2 * 3 = 1 + 2 + 3
# alors on peut construire les séquences de produit 12 :
# 12 = 2 * 6 -> 2 * 6 = 2 + 6 + 4*1 donc les séquences correspondantes sont de
# longueur              1 + 1 + 4  = 6
#                       1 + 2 + 4  = 7
# 12 = 3 * 4 -> 3 * 4 = 3 + 4 + 5*1, donc les séquences sont de longueur
#                       1 + 1 + 5  = 7
#                       1 + 2 + 5  = 8.
# et finalement 12 : {6, 7, 8}.
# En construisant le dictionnaire entier par entier, si un nouvel entier k apparaît
# dans l'entrée n, alors nécessairement m(k)=n.


from itertools import product
from math import floor, sqrt

def get_sums(n, mem):
    if n in mem:
        return mem[n]
    temp = {1}
    for i in range(2, floor(sqrt(n))+1):
        if n%i == 0:
            for a, b in product(get_sums(i, mem), get_sums(n//i, mem)):
                temp.add(a + b + n - (i + n//i))
    mem[n] = temp
    return temp

t0 = time()

threshold = 12000
vals = set(range(2, threshold+1))
min_n = set()
mem = {}
i = 2
while vals:
    for val in get_sums(i, mem):
        if val in vals:
            vals.remove(val)
            min_n.add(i)
    i += 1

print(sum(min_n),"\nTemps écoulé ",time()-t0)
